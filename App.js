import React from 'react';

// Context
import {AppContextProvider} from './AppContext';

// Navigator
import AppNavigator from './AppNavigator';

const App = () => {
  return (
    <AppContextProvider>
      <AppNavigator />
    </AppContextProvider>
  );
};

export default App;

import {config} from './ConfigConstants';

const HOST_NAME = config.host;

export const getData = async (resource, header) => {
  try {
    const response = await fetch(`${HOST_NAME}${resource}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...header,
      },
    });
    return response.status === 200 ? response.json() : false;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const postData = async (resource, data, header) => {
  const response = await fetch(`${HOST_NAME}${resource}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...header,
    },
    body: JSON.stringify(data),
  });

  return response.status === 200 ? response.json() : false;
};

export const getGeoPoints = async (geoString, distance) => {
  const distanceString = distance || 10;
  const resource = `arcgis/rest/services/Geoservicio_AND/MapServer/2/query?where=1=1&text=&objectIds=&time=&geometry=${geoString}&geometryType=esriGeometryPoint&inSR=4326&spatialRel=esriSpatialRelIntersects&distance=${distanceString}&units=esriSRUnit_Kilometer&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=4326&havingClause=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&returnExtentOnly=false&datumTransformation=&prerarameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=json`;
  const geoData = await getData(resource);
  return geoData && geoData.features;
};

export const deleteData = async (resource, header) => {
  try {
    const response = await fetch(`${HOST_NAME}${resource}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...header,
      },
    });
    return response.status === 200 ? response.json() : false;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getArcgisPoints = async (favorites, type) => {
  const attractionOrOperator = type === 'Atractivo';
  const favoritesOfTypeArray = favorites
    .filter(favorite => favorite.type === type)
    .map(favorite => `'${favorite.id_site}'`)
    .join(',');

  const favoritesResource = attractionOrOperator
    ? `2/query?where=id_atractivo+IN(${favoritesOfTypeArray})`
    : `1/query?where=id_operador+IN(${favoritesOfTypeArray})`;

  const favoritesResult = await getData(
    `arcgis/rest/services/Geoservicio_AND/MapServer/${favoritesResource}&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&havingClause=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=json`,
  );

  if (favoritesResult && favoritesResult.features) {
    const objectIds = favoritesResult.features
      .map(item => item.attributes.objectid)
      .join(',');

    const mediaResource = attractionOrOperator
      ? `2/queryRelatedRecords?objectIds=${objectIds}&relationshipId=4&outFields=id_multimedia%2Cid_atractivo%2Curl`
      : `1/queryRelatedRecords?objectIds=${objectIds}&relationshipId=1&outFields=id_multimedia%2Cid_operador%2Curl`;

    const mediaResult = await getData(
      `arcgis/rest/services/Geoservicio_AND/MapServer/${mediaResource}&definitionExpression=&returnGeometry=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnZ=false&returnM=false&gdbVersion=&datumTransformation=&f=json`,
    );

    if (mediaResult && mediaResult.relatedRecordGroups) {
      return favoritesResult.features.map(item => {
        const multimedia = mediaResult.relatedRecordGroups.find(
          mediaitem => item.attributes.objectid === mediaitem.objectId,
        );
        item.attributes.multimedia =
          multimedia.relatedRecords.map(media => media.attributes) || [];
        return item.attributes;
      });
    } else {
      return favoritesResult.features;
    }
  } else {
    return false;
  }
};

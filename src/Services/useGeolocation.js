import {useState, useEffect} from 'react';
import {Platform, PermissionsAndroid, ToastAndroid} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

const androidLocationPermission = async () => {
  if (Platform.OS === 'android' && Platform.Version < 23) {
    return true;
  }

  const hasPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (hasPermission) {
    return true;
  }

  const status = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (status === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  }

  if (status === PermissionsAndroid.RESULTS.DENIED) {
    ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    ToastAndroid.show(
      'Location permission revoked by user.',
      ToastAndroid.LONG,
    );
  }

  return false;
};

export default () => {
  const [hasLocationPermission, setLocationPermission] = useState(false);
  const [currentLocation, setLocation] = useState(false);

  useEffect(() => {
    const requestAuthorization = async () => {
      const result = await Geolocation.requestAuthorization('whenInUse');
      setLocationPermission(result === 'granted' ? true : false);
    };

    if (Platform.OS === 'android') {
      const androidPermission = androidLocationPermission();
      setLocationPermission(androidPermission);
    } else {
      requestAuthorization();
    }
  }, []);

  useEffect(() => {
    if (hasLocationPermission) {
      Geolocation.watchPosition(
        position => {
          setLocation(position);
          // return position;
        },
        error => {
          // See error code charts below.
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, distanceFilter: 30},
      );
    }

    return () => Geolocation.stopObserving();
  }, [hasLocationPermission]);

  return currentLocation;
};

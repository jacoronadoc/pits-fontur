export const checkFavorite = (favorites, point) =>
  favorites.find(favorite => {
    if (favorite.type === 'Atractivo' && point.id_atractivo) {
      return point.id_atractivo === favorite.id_site;
    }
    if (favorite.type === 'Operador' && point.id_operador) {
      return point.id_operador === favorite.id_site;
    }
    return false;
  });

export const stampToHour = timestamp => {
  let h = new Date(timestamp).getUTCHours();
  let m = new Date(timestamp).getUTCMinutes();

  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;

  return h + ':' + m;
};

export const getStampPlusHours = hours => {
  var dt = new Date();
  dt.setHours(dt.getUTCHours() + hours);
  return dt.getTime();
};

import React from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';

const renderOutsideTouchable = onTouch => {
  const view = <View style={{flex: 1, width: '100%'}} />;

  if (!onTouch) {
    return view;
  }

  return (
    <TouchableWithoutFeedback
      onPress={onTouch}
      style={{flex: 1, width: '100%'}}>
      {view}
    </TouchableWithoutFeedback>
  );
};

export {renderOutsideTouchable};

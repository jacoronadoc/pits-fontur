import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {WebView} from 'react-native-webview';

import {config} from '../Services/ConfigConstants';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Strings
import theme from '../Styles/_main';

const ApiServer = config.host;

export default ({route, navigation}) => {
  const {image} = route.params;
  return (
    <View style={styles.container}>
      <View style={styles.heroContainer}>
        <TouchableOpacity
          style={styles.closeBtn}
          onPress={() => navigation.goBack()}>
          <Icon name={'close'} size={20} color={'white'} />
        </TouchableOpacity>
      </View>
      <WebView
        style={styles.webView}
        originWhitelist={['*']}
        source={{uri: `${ApiServer}web/mobile-img360?image=${image}`}}
        onLoad={() => false}
        androidHardwareAccelerationDisabled={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  webView: {
    flex: 1,
    backgroundColor: 'black',
  },
  closeBtn: {
    width: 25,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

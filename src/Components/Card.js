import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';
import i18n from '../nls/i18n';

// Styles
import theme from '../Styles/_main';

//Resources
import ButtonStars from '../Resources/ButtonStars';
import CategoriesIcons from '../Resources/CategoriesIcons';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);
const lang = i18n.t('filterMap');

export default ({
  containerStyle,
  descriptionLines,
  descriptionStyle,
  detailsMethod,
  favoriteChecked,
  favoriteMethod,
  favoriteVisible,
  infoContainerStyle,
  item,
  labelStyle,
  labelTop,
  shadow,
  starsLabel,
  starsValue,
  titleLines,
  titleStyle,
}) => {
  const {Card: cardStyle} = theme;
  const favoriteIcon = favoriteChecked ? 'heart' : 'heart-outline';
  const styleContainer = containerStyle || {width: '100%', height: 100};
  const styleInfoContainer = infoContainerStyle || {};
  const styleShadow = shadow ? cardStyle.shadow : {};
  const styleTitle = titleStyle || {};
  const styleDescription = descriptionStyle || {};
  const styleLabel = labelStyle || {};

  return (
    <View style={theme.add(cardStyle.container, styleShadow, styleContainer)}>
      {favoriteVisible && (
        <TouchableOpacity
          onPress={() => favoriteMethod(item, favoriteChecked)}
          style={cardStyle.favoriteButton}>
          <Icon name={favoriteIcon} size={20} color={theme.palette.secondary} />
        </TouchableOpacity>
      )}
      <ImageBackground
        source={{uri: item.image}}
        style={theme.add(cardStyle.image, {width: styleContainer.height - 10})}
      />
      <View style={theme.add(cardStyle.infoContainer, styleInfoContainer)}>
        {starsValue && (
          <View style={theme.flexWrap}>
            <ButtonStars
              value={starsValue}
              label={starsLabel}
              small={true}
              unbordered={true}
              onChange={() => false}
            />
          </View>
        )}
        {item.label && labelTop && (
          <Text style={theme.add(cardStyle.infoLabel, styleLabel)}>
            {item.label}
          </Text>
        )}
        <Text
          numberOfLines={titleLines}
          style={theme.add(cardStyle.infoTitle, styleTitle)}>
          {item.title}
        </Text>
        {item.label && !labelTop && (
          <Text style={theme.add(cardStyle.infoLabel, styleLabel)}>
            {item.label}
          </Text>
        )}
        <Text
          numberOfLines={descriptionLines}
          style={theme.add(cardStyle.infoText, styleDescription)}>
          {item.description}
        </Text>
        <View style={cardStyle.footer}>
          <CategoriesIcons
            allCategories={item.allCategories}
            mainCategory={item.categoria_principal}
          />
          {detailsMethod && (
            <TouchableOpacity
              onPress={() => detailsMethod(item)}
              style={cardStyle.detailsBtn}>
              <Text style={cardStyle.infoText}>{lang.view}</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

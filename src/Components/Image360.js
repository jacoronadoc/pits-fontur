import React from 'react';
import {View, StyleSheet} from 'react-native';

// eslint-disable-next-line prettier/prettier
import {
  ViroScene,
  ViroText,
  Viro360Image,
  ViroVRSceneNavigator,
} from '@viro-community/react-viro';

const renderImage = () => (
  <ViroScene>
    <Viro360Image source={require('../assets/images/image360.jpg')} />
    <ViroText
      text={'Hello World!'}
      scale={[0.5, 0.5, 0.5]}
      position={[0, 0, -1]}
      style={styles.helloWorldTextStyle}
    />
  </ViroScene>
);

const Image360 = () => {
  const _onBackgroundPhotoLoadEnd = () => {
    console.log('images/image360.jpg');
    console.log('image loaded...');
  };
  return (
    <View style={styles.view}>
      <ViroVRSceneNavigator
        vrModeEnabled={false}
        initialScene={{scene: renderImage}}
        style={styles.container}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#f00',
  },
  container: {
    flex: 1,
    backgroundColor: 'blue',
    width: '100%',
    height: '100%',
  },
  image: {
    backgroundColor: 'blue',
    width: 300,
    height: 300,
    borderWidth: 1,
    borderColor: 'blue',
  },
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 60,
    color: '#000',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});

export default Image360;

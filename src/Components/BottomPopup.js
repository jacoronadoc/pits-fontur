import React, {useState, useImperativeHandle} from 'react';
import {Modal, View, Text} from 'react-native';

import {renderOutsideTouchable} from './BottomPopup.methods';
import styles from './BottomPopup.styles';

const BottomPopup = React.forwardRef((props, ref) => {
  const [visible, setVisible] = useState(false);

  useImperativeHandle(ref, () => ({
    show: () => setVisible(true),
    close: () => setVisible(false),
  }));

  return (
    <Modal
      animationType={'fade'}
      transparent={true}
      visible={visible}
      onRequestClose={() => setVisible(false)}>
      <View onPress={() => setVisible(false)} style={styles.container}>
        {renderOutsideTouchable(() => setVisible(false))}
        <View style={styles.content}>{props.children}</View>
      </View>
    </Modal>
  );
});

export default BottomPopup;

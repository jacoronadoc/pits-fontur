// Services
import {getData, postData, deleteData} from '../Services/dataServices';

const setFavorite = async (item, userToken) => {
  const type = item.id_atractivo ? 'Atractivo' : 'Operador';
  const id_site = item.id_atractivo ? item.id_atractivo : item.id_operador;
  /*
  console.log({
    name: item.title,
    type,
    id_site,
  }); */

  const response = await postData(
    'api/favorites/create',
    {
      type,
      id_site,
    },
    {
      'x-access-token': userToken,
    },
  );

  return response;
};

const getFavorites = async userToken => {
  const response = await getData('api/favorites/sites', {
    'x-access-token': userToken,
  });

  return response;
};

const deleteFavorite = async (id, userToken) => {
  const response = await deleteData(`api/favorites/site/${id}`, {
    'x-access-token': userToken,
  });

  return response;
};

export {setFavorite, getFavorites, deleteFavorite};

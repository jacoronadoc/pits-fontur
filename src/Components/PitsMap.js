import React, {useState, useRef, useEffect, useContext} from 'react';
import {
  View,
  Text,
  Image,
  Keyboard,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';
import i18n from '../nls/i18n';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);
const lang = i18n.t('map');

// Methods
import setStorage from './PitsMap.setStorage';
import getCards from './PitsMap.getCards';
import {
  getFavorites,
  setFavorite,
  deleteFavorite,
} from './PitsMap.handleFavorites';

// Components
import BottomPopup from './BottomPopup';
import FilterMap from './FilterMap';
import Carousel from './Carousel';

import styles from './PitsMap.styles';

import {config} from '../Services/ConfigConstants';

// Context
import {AppContext} from '../../AppContext';

const ApiServer = config.host;

export default ({navigation}) => {
  const {appData, handleAppData} = useContext(AppContext);
  const {width: windowWidth} = useWindowDimensions();

  const [features, setFeatures] = useState([]);
  const [countFeatures, setCountFeatures] = useState(0);
  const [mobileUIVisible, setMobileUIVisible] = useState(true);
  const [seconWebview, enable2Webview] = useState(false);

  const [pitsMobileData, setPitsMobileData] = useState({
    flags: {
      divDirections: true,
      hideDirections: true,
      hideLocate: true,
    },
    filter: {
      attraction: true,
      attractionCategory: [],
      buffer: 1,
      keyWord: '',
      maxValue: 2000,
      minValue: 0,
      operator: false,
      operatorCategory: [],
    },
    point: null,
  });

  const favoriteAction = async (item, itemChecked) => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      const result = !itemChecked
        ? await setFavorite(item, appData.user.accessToken)
        : await deleteFavorite(itemChecked.id, appData.user.accessToken);
      if (result) {
        const favoritesList = await getFavorites(appData.user.accessToken);
        if (favoritesList) {
          handleAppData({
            ...appData,
            favorites: favoritesList,
          });
        }
      }
    }
  };

  const accountAction = () => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      return navigation.navigate('Account');
    }
  };

  const detailsAction = point => navigation.navigate('Point Details', {point});

  let getWebview = null;
  let setWebview = null;
  let filterPopup = useRef();

  const setPitsMobileObject = obj => {
    const newPitsMobileData = {
      ...pitsMobileData,
      ...obj,
    };
    setPitsMobileData(newPitsMobileData);
    return newPitsMobileData;
  };

  const handlePitsMobileData = obj => {
    const newObj = setPitsMobileObject(obj);
    setFeatures([]);
    setStorage(setWebview, newObj);
  };

  const handleSwitchRoute = value => {
    const newData = {
      ...pitsMobileData,
      flags: {
        divDirections: true,
        hideDirections: value,
        hideLocate: true,
      },
    };

    setPitsMobileObject(newData);
    setStorage(setWebview, newData);
  };

  const getMessage = event => {
    const dataPost = JSON.parse(event.nativeEvent.data);
    if (typeof dataPost.mobileUIVisible !== 'undefined') {
      setMobileUIVisible(dataPost.mobileUIVisible);
    }

    if (dataPost.features.length > 100) {
      setFeatures(dataPost.features.slice(0, 100));
    } else {
      setFeatures(dataPost.features);
    }

    const point =
      dataPost.point && dataPost.point.latitude
        ? {
            latitude: dataPost.point.latitude,
            longitude: dataPost.point.longitude,
          }
        : null;

    setPitsMobileObject({
      point,
      filter: dataPost.filter,
    });
  };

  useEffect(() => {
    // obtener favoritos si existe usuario
    if (!appData.user) {
      return false;
    }
    (async () => {
      const favoritesList = await getFavorites(appData.user.accessToken);
      if (favoritesList) {
        handleAppData({
          ...appData,
          favorites: favoritesList,
        });
      }
    })();
  }, [appData.user]);

  useEffect(() => {
    if (features.length !== countFeatures) {
      setCountFeatures(features.length);
      setMobileUIVisible(true);
      Keyboard.dismiss();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [features]);

  return (
    <View style={styles.container}>
      <WebView
        ref={ref => (getWebview = ref)}
        style={styles.getWebview}
        originWhitelist={['*']}
        source={{uri: `${ApiServer}web/mobile-explore-map`}}
        // source={{uri: 'http://192.168.10.21:3000/web/mobile-explore-map'}}
        onMessage={getMessage}
        onLoad={() => enable2Webview(true)}
        androidHardwareAccelerationDisabled={false}
      />
      <View style={styles.setWebview}>
        {seconWebview && (
          <WebView
            ref={ref => (setWebview = ref)}
            originWhitelist={['*']}
            source={{
              uri: `${ApiServer}web/mobile-explore-map?nomap=true`,
              // uri: 'http://192.168.10.21:3000/web/mobile-explore-map?nomap=true',
            }}
            androidHardwareAccelerationDisabled={true}
          />
        )}
      </View>
      <View style={styles.mapHeaderContainer}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => navigation.navigate('Admin')}>
          <Image source={require('../assets/images/icon-info.png')} />
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={1} onPress={accountAction}>
          <Image source={require('../assets/images/icon-account.png')} />
        </TouchableOpacity>
      </View>
      {!pitsMobileData.flags.hideDirections && (
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => handleSwitchRoute(true)}
          style={styles.btnRutas}>
          <Text style={styles.touchableOpacityText}>
            <Icon name={'close'} size={20} color={'black'} />
          </Text>
        </TouchableOpacity>
      )}

      {seconWebview && mobileUIVisible && pitsMobileData.flags.hideDirections && (
        <>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => handleSwitchRoute(false)}
            style={styles.btnRutas}>
            <Text style={styles.touchableOpacityText}>{lang.routes}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={filterPopup.current.show}
            style={styles.touchableOpacityStyle}>
            <Text style={styles.touchableOpacityText}>{lang.filters}</Text>
          </TouchableOpacity>
          <View style={styles.cardsContainer}>
            <Carousel
              dots={true}
              pages={features}
              frameWidth={windowWidth - 20}>
              {features.length > 0 &&
                getCards(
                  features,
                  appData.favorites,
                  windowWidth,
                  favoriteAction,
                  detailsAction,
                )}
            </Carousel>
          </View>
        </>
      )}
      <BottomPopup ref={filterPopup}>
        <FilterMap
          modal={filterPopup}
          filterData={pitsMobileData.filter}
          filterAction={handlePitsMobileData}
        />
      </BottomPopup>
    </View>
  );
};

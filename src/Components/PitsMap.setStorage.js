export default (webview, pitsMobileData) => {
  const pitsMobileDataString = JSON.stringify(pitsMobileData);
  const injected = `
        localStorage.setItem('pitsMobileData', '${pitsMobileDataString}');
        `;
  webview.injectJavaScript(injected);
};

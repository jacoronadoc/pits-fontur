import React, {useState, useEffect} from 'react';
import {Text, StyleSheet} from 'react-native';
import CompassHeading from 'react-native-compass-heading';
import {useIsFocused} from '@react-navigation/native';

// Strings
import i18n from '../nls/i18n';

import {
  ViroARScene,
  ViroText,
  ViroFlexView,
  ViroImage,
  ViroARSceneNavigator,
} from '@viro-community/react-viro';

// Services
import {config} from '../Services/ConfigConstants';
import {getGeoPoints} from '../Services/dataServices';
import useGeolocation from '../Services/useGeolocation';

const ApiServer = config.host;
const i18nARScene = i18n.t('arScene');

const geoSanAndres = {
  minx: -81.14,
  maxx: -81.92,
  miny: 12.33,
  maxy: 13.53,
};
const geoColombia = {
  minx: -66.76,
  maxx: -79.51,
  miny: -4.41,
  maxy: 12.81,
};

const _latLongToMerc = (lat_deg, lon_deg) => {
  var lon_rad = (lon_deg / 180.0) * Math.PI;
  var lat_rad = (lat_deg / 180.0) * Math.PI;
  var sm_a = 6378137.0;
  var xmeters = sm_a * lon_rad;
  var ymeters = sm_a * Math.log((Math.sin(lat_rad) + 1) / Math.cos(lat_rad));
  return {x: xmeters, y: ymeters};
};

const _transformPointToAR = (lat, long, geoLocation, compassInfo) => {
  const {latitude, longitude} = geoLocation.coords;
  // const latitude = 4.598193;
  // const longitude = -74.075927;

  var objPoint = _latLongToMerc(lat, long);
  var devicePoint = _latLongToMerc(latitude, longitude);
  var objFinalPosZ = objPoint.y - devicePoint.y;
  var objFinalPosX = objPoint.x - devicePoint.x;
  return rotate(0, 0, objFinalPosX, objFinalPosZ, compassInfo);
};

function rotate(cx, cy, x, y, angle) {
  var radians = (Math.PI / 180) * (360 - angle),
    cos = Math.cos(radians),
    sin = Math.sin(radians),
    nx = cos * (x - cx) + sin * (y - cy) + cx,
    ny = cos * (y - cy) - sin * (x - cx) + cy;

  return {x: nx, z: -ny};
}

const PointCard = (point, geolocation, compassInfo) => {
  const {objectid, nombre, direccion, categoria_principal} = point.attributes;
  const {x, y} = point.geometry;
  const position = _transformPointToAR(y, x, geolocation, compassInfo);

  return (
    <ViroFlexView
      key={objectid}
      style={styles.cardContainer}
      position={[position.x, 0, position.z]}
      scale={[1, 1, 1]}
      transformBehaviors={['billboard']}
      height={2}
      width={4.5}>
      <ViroImage
        source={{
          uri: `${ApiServer}multimedia_fontur/tarjetas/${categoria_principal}.png`,
        }}
        width={1}
        height={1}
      />
      <ViroFlexView width={3} style={styles.descContainer}>
        <ViroText
          style={styles.prodTitleText}
          text={nombre}
          width={4}
          height={0.5}
        />
        <ViroText style={styles.prodDescriptionText} text={direccion} />
      </ViroFlexView>
    </ViroFlexView>
  );
};

const verifyColombia = (lat, lng) => {
  return (
    (lng < geoColombia.minx &&
      lng > geoColombia.maxx &&
      lat > geoColombia.miny &&
      lat < geoColombia.maxy) ||
    (lng < geoSanAndres.minx &&
      lng > geoSanAndres.maxx &&
      lat > geoSanAndres.miny &&
      lat < geoSanAndres.maxy)
  );
};

const SceneAR = () => {
  const [compassValue, setCompassValue] = useState(-1);
  const [points, setPoints] = useState([]);
  const [isColombia, setIsColombia] = useState(undefined);
  const geolocation = useGeolocation();

  const showErrorPoints = isDeviceInColombia => {
    if (!geolocation) {
      return false;
    }

    const {latitude, longitude} = geolocation.coords;

    const errorPoint = (id, x, y) =>
      Object.create({
        attributes: {
          objectid: id,
          nombre: isDeviceInColombia
            ? i18nARScene.errorTitle
            : i18nARScene.noColombiaTitle,
          direccion: isDeviceInColombia
            ? i18nARScene.errorMessage
            : i18nARScene.noColombiaMessage,
          categoria_principal: '01',
        },
        geometry: {
          x,
          y,
        },
      });

    return [
      errorPoint(1, longitude - 0.00003, latitude - 0.00003),
      errorPoint(2, longitude + 0.00003, latitude + 0.00003),
      errorPoint(3, longitude - 0.00003, latitude + 0.00003),
      errorPoint(4, longitude + 0.00003, latitude - 0.00003),
    ];
  };

  useEffect(() => {
    CompassHeading.stop();
  }, [compassValue]);

  useEffect(() => {
    if (!geolocation) {
      return false;
    }

    const {latitude, longitude} = geolocation.coords;
    const degree_update_rate = 3;

    // accuracy on android will be hardcoded to 1
    // since the value is not available.
    // For iOS, it is in degrees
    CompassHeading.start(degree_update_rate, ({heading, accuracy}) => {
      setCompassValue(heading);
    });

    const fetchPoints = async () => {
      const geoPoints = await getGeoPoints(`${longitude}, ${latitude}`, 1);
      // const geoPoints = await getGeoPoints('-74.075927, 4.598193', 1);
      if (geoPoints && geoPoints.length > 0) {
        setPoints(geoPoints);
      } else {
        setIsColombia(verifyColombia(latitude, longitude));
      }
    };

    fetchPoints();

    return () => {
      CompassHeading.stop();
    };
  }, [geolocation]);

  useEffect(() => {
    if (isColombia !== undefined) {
      setPoints(showErrorPoints(isColombia));
    }
  }, [isColombia]);

  return (
    <ViroARScene>
      {points.length > 0 &&
        points.map(point => PointCard(point, geolocation, compassValue))}
    </ViroARScene>
  );
};

export default ({navigation}) => {
  const isFocused = useIsFocused();

  if (!isFocused) return <Text>Isn't focused</Text>;

  return (
    <ViroARSceneNavigator
      autofocus={true}
      initialScene={{
        scene: SceneAR,
      }}
      style={styles.f1}
    />
  );
};

var styles = StyleSheet.create({
  f1: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  prodTitleText: {
    fontFamily: 'sans-serif-light',
    fontSize: 30,
    color: '#333',
    textAlignVertical: 'center',
    textAlign: 'left',
    flexWrap: 'wrap',
  },
  prodDescriptionText: {
    fontFamily: 'sans-serif-light',
    fontSize: 20,
    color: '#333',
    textAlignVertical: 'center',
    textAlign: 'left',
    flex: 1,
    flexWrap: 'wrap',
  },
  cardContainer: {
    flexDirection: 'row',
    backgroundColor: '#ffffffa1',
    padding: 0.2,
  },
  descContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});

import React, {useRef, useEffect} from 'react';
import {ScrollView, StyleSheet, View, Animated} from 'react-native';

export default ({dots, pages, frameWidth, children}) => {
  const scrollView = useRef();
  const scrollX = useRef(new Animated.Value(0)).current;

  const shouldShowDots = dots && pages.length <= 10;
  const dotItems = Array(pages.length).fill(1);

  useEffect(() => {
    scrollView.current?.scrollTo({x: 0, y: 0, animated: false});
  }, [pages]);

  return (
    <View style={styles.scrollContainer}>
      <ScrollView
        ref={scrollView}
        horizontal={true}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  x: scrollX,
                },
              },
            },
          ],
          {useNativeDriver: false},
        )}
        scrollEventThrottle={1}>
        {children}
      </ScrollView>
      {shouldShowDots && (
        <View style={styles.indicatorContainer}>
          {dotItems.map((page, pageIndex) => {
            const width = scrollX.interpolate({
              inputRange: [
                frameWidth * (pageIndex - 1),
                frameWidth * pageIndex,
                frameWidth * (pageIndex + 1),
              ],
              outputRange: [8, 16, 8],
              extrapolate: 'clamp',
            });
            return (
              <Animated.View
                key={pageIndex}
                style={[styles.normalDot, {width}]}
              />
            );
          })}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  normalDot: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: 'silver',
    marginHorizontal: 4,
  },
  indicatorContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 20,
  },
});

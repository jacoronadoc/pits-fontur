import React from 'react';

// Language
import i18n from '../nls/i18n';
const i18nFilter = i18n.t('filterMap');

// Resources
import ButtonIcon from '../Resources/ButtonIcon';

export default ({width, onChange, checkedItems}) => {
  const categoriesButtonIcons = [
    {
      text: i18nFilter.nature,
      icon: 'nature',
      name: '01',
    },
    {
      text: i18nFilter.culturalTourism,
      icon: 'camera',
      name: '02',
    },
    {
      text: i18nFilter.archeologicalTourism,
      icon: 'archaeological',
      name: '05',
    },
    {
      text: i18nFilter.churches,
      icon: 'church',
      name: '06',
    },
    {
      text: i18nFilter.sunAndBeach,
      icon: 'beach',
      name: '04',
    },
    {
      text: i18nFilter.lgbtiTurism,
      image: require('../assets/images/lgbti-icon.png'),
      name: '08',
    },
    {
      text: i18nFilter.themeParks,
      icon: 'theme-park',
      name: '07',
    },
    {
      text: i18nFilter.nationalParks,
      icon: 'natural-parks',
      name: '09',
    },
  ];

  const Categories = categoriesButtonIcons.map((item, x) => {
    const theValue = checkedItems.indexOf(item.name) >= 0;
    return (
      <ButtonIcon
        parentStyle={{width: width / 2 - 20}}
        key={x}
        {...item}
        value={theValue}
        onChange={onChange}
      />
    );
  });

  return Categories;
};

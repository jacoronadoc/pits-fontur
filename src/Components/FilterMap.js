import React, {useContext, useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import i18n from '../nls/i18n';

// Context
import {AppContext} from '../../AppContext';

// Styles
import theme from '../Styles/_main';

// Resources
import FilterMapCategories from './FilterMap.Categories';
import FilterMapTourOperators from './FilterMap.TourOperators';
import SwitchLabel from '../Resources/SwitchLabel';
import RangeSlider from '../Resources/RangeSlider';
import TextInputLabel from '../Resources/TextInputLabel';
import ButtonStars from '../Resources/ButtonStars';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const i18nFilter = i18n.t('filterMap');

export default ({modal, filterData, filterAction}) => {
  const {safeIn} = theme;
  const {appData, setAppData} = useContext(AppContext);
  const [distanceRange, setDistanceRange] = useState(0);
  const [filterObject, setFilterObject] = useState(filterData);
  const [attractionCategories, setAttractionCategories] = useState(
    filterData.attractionCategory,
  );

  const buildFilterObject = obj => {
    const newFilterObject = {
      ...filterObject,
      ...obj,
    };
    setFilterObject(newFilterObject);
  };

  const handleAttractionCategories = item => {
    const [key, value] = Object.entries(item)[0];
    const newAttractionCategories = [...filterObject.attractionCategory];

    if (!value && newAttractionCategories.includes(key)) {
      const index = newAttractionCategories.indexOf(key);
      newAttractionCategories.splice(index, 1);
      buildFilterObject({attractionCategory: newAttractionCategories});
    }

    if (value && !newAttractionCategories.includes(key)) {
      newAttractionCategories.push(key);
      buildFilterObject({attractionCategory: newAttractionCategories});
    }
  };

  const handleDistanceRange = min => {
    setDistanceRange(min);
  };

  useEffect(() => {
    setAttractionCategories(filterObject.attractionCategory);
  }, [filterObject]);

  useEffect(() => {
    buildFilterObject({buffer: distanceRange});
  }, [distanceRange]);

  return (
    <View>
      <View
        style={{
          minHeight: deviceHeight * 0.7,
        }}>
        <ScrollView
          style={{
            flex: 1,
            ...safeIn.MdX,
          }}>
          <Text style={theme.h3}>{i18nFilter.filter}</Text>
          <SwitchLabel
            accessibilityRole="button"
            text={i18nFilter.touristAttractions}
            name="attraction"
            value={filterObject.attraction}
            onChange={buildFilterObject}
          />
          {filterObject.attraction && (
            <>
              <Text style={theme.h4}>{i18nFilter.categories}</Text>
              <View
                style={{
                  ...theme.flexWrap,
                  ...safeIn.SmY,
                  justifyContent: 'space-between',
                }}>
                <FilterMapCategories
                  width={deviceWidth}
                  checkedItems={attractionCategories}
                  onChange={item => handleAttractionCategories(item)}
                />
              </View>
            </>
          )}
          {/*
          <Text style={safeIn.Y}>{`Bienvenido: ${appData.user}`}</Text>
          <Text style={theme.h4}>{i18nFilter.rate}</Text>
          <View
            style={{
              ...theme.flexWrap,
              ...safeIn.SmY,
              justifyContent: 'space-between',
            }}>
            <ButtonStars onChange={() => null} value={5} />
            <ButtonStars onChange={() => null} value={4} />
            <ButtonStars onChange={() => null} value={3} />
            <ButtonStars onChange={() => null} value={2} />
          </View>
          */}
          <SwitchLabel
            accessibilityRole="button"
            text={i18nFilter.tourOperators}
            name="operator"
            value={filterObject.operator}
            onChange={buildFilterObject}
          />
          {filterObject.operator && (
            <>
              <Text style={theme.h4}>{i18nFilter.categories}</Text>
              <FilterMapTourOperators 
                onChange={buildFilterObject}
                checkedItems={filterObject.operatorCategory}
              />
            </>
          )}
          <Text style={theme.h4}>{i18nFilter.distance}</Text>
          <Text
            style={{
              ...safeIn.SmY,
              textAlign: 'center',
            }}>{`${distanceRange} km`}</Text>
          <RangeSlider
            valueLow={filterObject.buffer}
            onChange={min => handleDistanceRange(min)}
            hideLabel={true}
          />
          <Text style={theme.h4}>{i18nFilter.price}</Text>
          <View
            style={{
              ...theme.flexRow,
              ...safeIn.SmY,
            }}>
            <TextInputLabel
              name="minValue"
              label={i18nFilter.priceFrom}
              type="numeric"
              value={filterObject.minValue.toString()}
              onChange={buildFilterObject}
            />
            <View style={safeIn.SmX} />
            <TextInputLabel
              name="maxValue"
              label={i18nFilter.priceTo}
              type="numeric"
              value={filterObject.maxValue.toString()}
              onChange={buildFilterObject}
            />
          </View>
        </ScrollView>
      </View>
      <View
        style={{
          ...theme.flexWrap,
          ...theme.flexSpaceBetween,
          ...safeIn.Y,
          ...safeIn.MdX,
          backgroundColor: theme.palette.inverse2,
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={modal.current.close}
          style={theme.button}>
          <Text style={theme.buttonText}>{i18nFilter.clean}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            filterAction({
              filter: filterObject,
            });
            modal.current.close();
          }}
          style={{
            ...theme.button,
            ...theme.buttonPrimary,
          }}>
          <Text style={theme.buttonTextPrimary}>{i18nFilter.submit}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

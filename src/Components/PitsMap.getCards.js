import React from 'react';

// Resources
import Card from './Card';

// Services
import {checkFavorite} from '../Services/checkFavorite';

// Strings
import i18n from '../nls/i18n';
const locale = i18n.currentLocale();

export default (
  features,
  favorites,
  windowWidth,
  favoriteAction,
  detailsAction,
) =>
  features.map((feature, itemIndex) => {
    feature.image =
      (feature.multimedia && feature.multimedia[0].url) ||
      'https://picsum.photos/id/1015/300/200.jpg';
    feature.label = feature.nombre_depto;
    feature.title = feature.nombre;
    feature.description =
      feature[`descripcion_${locale}`] || feature.descripcion;

    const isFavorite = checkFavorite(favorites, feature);

    return (
      <Card
        key={itemIndex}
        item={feature}
        containerStyle={{
          width: windowWidth - 30,
          height: 150,
          marginHorizontal: 5,
        }}
        titleLines={1}
        descriptionLines={2}
        starsValue={3}
        starsLabel="3.5"
        favoriteVisible={true}
        favoriteChecked={isFavorite}
        favoriteMethod={favoriteAction}
        detailsMethod={detailsAction}
        shadow={true}
      />
    );
  });

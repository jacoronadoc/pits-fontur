import {StyleSheet, Platform} from 'react-native';

import theme from '../Styles/_main';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  // eslint-disable-next-line react-native/no-color-literals
  getWebview: {
    backgroundColor: 'rgb(207, 231, 197)',
    flex: 1,
  },
  setWebview: {
    display: 'none',
    height: 0,
  },
  touchableOpacityText: {
    color: theme.palette.primary,
  },
  touchableOpacityStyle: {
    ...theme.button,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 100,
    right: '5%',
  },
  touchableOpacityStyleLeft: {
    ...theme.button,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 100,
    left: '5%',
  },
  mapHeaderContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    justifyContent: 'space-between',
    top: 50,
    left: '5%',
    width: '90%',
    height: Platform.OS === 'ios' ? 0 : 'auto',
  },
  card: {
    flex: 1,
    marginVertical: 4,
    marginHorizontal: 16,
    borderRadius: 5,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textContainer: {
    backgroundColor: 'rgba(0,0,0, 0.7)',
    paddingHorizontal: 24,
    paddingVertical: 8,
    borderRadius: 5,
  },
  infoText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  btnRutas: {
    ...theme.button,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 100,
    left: '5%',
  },
  cardsContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    position: 'absolute',
    bottom: 0,
  },
});

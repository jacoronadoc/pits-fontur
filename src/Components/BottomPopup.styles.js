import {Dimensions, StyleSheet} from 'react-native';

const deviceHeight = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000AA',
    justifyContent: 'flex-end',
  },
  content: {
    backgroundColor: 'white',
    width: '100%',
    borderTopRightRadius: 26,
    borderTopLeftRadius: 26,
    paddingTop: 20,
    maxHeight: deviceHeight * 0.9,
  },
});

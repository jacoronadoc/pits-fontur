import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import BouncyCheckbox from 'react-native-bouncy-checkbox';

// Language
import i18n from '../nls/i18n';
const i18nFilter = i18n.t('filterMap');

// Styles
import theme from '../Styles/_main';

const categoriesCheckboxes = [
  {
    text: i18nFilter.agencies,
    name: '21',
  },
  {
    text: i18nFilter.hotels,
    name: '22',
  },
  {
    text: i18nFilter.gastronomyBars,
    name: '23',
  },
  {
    text: i18nFilter.carRental,
    name: '24',
  },
  {
    text: i18nFilter.operatorsInFreeZones,
    name: '25',
  },
  {
    text: i18nFilter.travelSavingsCompanies,
    name: '26',
  },
  {
    text: i18nFilter.tourismServicesInParks,
    name: '27',
  },
  {
    text: i18nFilter.touristicRepOffices,
    name: '28',
  },
  {
    text: i18nFilter.tourismGuide,
    name: '29',
  },
  {
    text: i18nFilter.fairsConventionsOperators,
    name: '30',
  },
  {
    text: i18nFilter.transportation,
    name: '31',
  },
  {
    text: i18nFilter.industrialUsersInFreeZones,
    name: '32',
  },
  {
    text: i18nFilter.timesharePromoters,
    name: '33',
  },
];

export default ({onChange, checkedItems}) => {
  const [categories, setCategories] = useState(checkedItems);

  useEffect(() => {
    onChange({
      operatorCategory: categories,
    });
  }, [categories]);

  const Categories = categoriesCheckboxes.map((item, x) => {
    const theValue = checkedItems.indexOf(item.name) >= 0;
    return (
      <View
        style={{
          marginBottom: 7,
          paddingHorizontal: 3,
        }}>
        <BouncyCheckbox
          isChecked={theValue}
          fillColor={theme.palette.primary}
          unfillColor={theme.palette.default}
          iconStyle={{borderColor: theme.palette.primary}}
          text={item.text}
          onPress={isChecked => {
            const categoryIndex = categories.indexOf(item.name);
            const newCategories = [...categories];
            if (isChecked && categoryIndex < 0) {
              newCategories.push(item.name);
            } else {
              newCategories.splice(categoryIndex, 1);
            }
            setCategories(newCategories);
          }}
          textStyle={{
            textDecorationLine: 'none',
          }}
        />
      </View>
    );
  });

  return Categories;
};

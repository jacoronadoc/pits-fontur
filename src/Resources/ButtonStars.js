import React, {useState} from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Styles
import theme from '../Styles/_main';
import styles from './ButtonIcon.styles';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default ({name, value, onChange, label, small, unbordered}) => {
  const {palette, buttonSwitchOff, buttonSwitchOn} = theme;
  const [checked, setChecked] = useState(false);
  const iconSize = small ? 11 : 20;
  let styleActive = checked ? buttonSwitchOn : buttonSwitchOff;
  let buttonBorder = unbordered ? null : styleActive;
  let style = small
    ? styles.buttonIconSm
    : {
        paddingTop: 10,
      };

  const onPressAction = isActive => {
    setChecked(isActive);
    onChange({[name]: isActive});
  };

  const stars = [];
  for (let i = 1; i <= 5; i++) {
    const colored = value >= i ? palette.primary : palette.inverse3;
    stars.push(
      <Icon
        name="star"
        style={small ? styles.iconSmall : styles.iconNormal}
        key={i}
        size={iconSize}
        color={colored}
      />,
    );
  }

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={() => onPressAction(!checked)}
      style={theme.add(styles.buttonIcon, style, buttonBorder)}>
      {label && <Text style={styles.label}> {label}</Text>}
      {stars}
    </TouchableOpacity>
  );
};

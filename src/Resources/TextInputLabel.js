import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

//Styles
import theme from '../Styles/_main';

const TextInputLabel = ({
  label,
  name,
  value,
  type,
  onChange,
  placeholder = '',
}) => {
  return (
    <View style={styles.container}>
      <Text>{label}</Text>
      <TextInput
        onChangeText={val => {
          onChange({[name]: val});
        }}
        value={value}
        placeholder={placeholder}
        keyboardType={type}
        style={styles.textInput}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...theme.buttonSwitchOff,
    ...theme.button,
    alignItems: 'flex-start',
    height: 'auto',
    flex: 1,
    marginBottom: 15,
  },
  textInput: {
    color: theme.palette.inverse4,
    minHeight: 45,
  },
});

export default TextInputLabel;

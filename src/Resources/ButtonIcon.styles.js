import {StyleSheet} from 'react-native';

import theme from '../Styles/_main';

const {button} = theme;

export default StyleSheet.create({
  buttonIcon: {
    ...button,
    ...theme.flexWrap,
    height: 'auto',
    alignItems: 'center',
    paddingVertical: 5,
    marginBottom: 15,
  },
  buttonIconSm: {
    minHeight: 15,
    paddingHorizontal: 2,
    paddingVertical: 3,
    marginBottom: 5,
  },
  buttonIconTextContainer: {
    flex: 2,
  },
  image: {
    height: 25,
    width: 25,
  },
  label: {
    color: theme.palette.primary,
    marginHorizontal: 5,
    fontWeight: 'bold',
  },
  iconNormal: {
    marginHorizontal: 3,
  },
  iconSmall: {
    marginHorizontal: 1,
  },
});

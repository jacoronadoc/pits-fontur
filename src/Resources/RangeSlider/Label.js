import React, {memo} from 'react';
import {View, Text, StyleSheet} from 'react-native';

// Styles
import theme from '../../Styles/_main';

const Label = ({text, ...restProps}) => {
  return (
    <View style={styles.root} {...restProps}>
      <Text style={styles.text}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 8,
    backgroundColor: theme.palette.primary,
    borderRadius: 4,
  },
  text: {
    fontSize: 16,
    color: theme.palette.default,
  },
});

export default memo(Label);

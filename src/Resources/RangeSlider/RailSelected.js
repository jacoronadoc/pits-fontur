import React, {memo} from 'react';
import {StyleSheet, View} from 'react-native';

// Styles
import theme from '../../Styles/_main';

const RailSelected = () => {
  return <View style={styles.root} />;
};

export default memo(RailSelected);

const styles = StyleSheet.create({
  root: {
    height: 4,
    backgroundColor: theme.palette.primary,
    borderRadius: 2,
  },
});

import React, {useCallback, useState} from 'react';
import RangeSlider from 'rn-range-slider';

import Thumb from './Thumb';
import Rail from './Rail';
import RailSelected from './RailSelected';
import Label from './Label';
import Notch from './Notch';

export default ({
  valueLow = 0,
  valueHigh = 10,
  hideRange = true,
  showFloatingLabel = true,
  hideLabel = true,
  inputMin = 0,
  inputMax = 10,
  inputStep = 0.1,
  onChange,
}) => {
  const [rangeDisabled, setRangeDisabled] = useState(hideRange);
  const [low, setLow] = useState(valueLow);
  const [high, setHigh] = useState(valueHigh);
  const [min, setMin] = useState(inputMin);
  const [max, setMax] = useState(inputMax);
  const [floatingLabel, setFloatingLabel] = useState(showFloatingLabel);

  const renderThumb = useCallback(() => <Thumb />, []);
  const renderRail = useCallback(() => <Rail />, []);
  const renderRailSelected = useCallback(() => <RailSelected />, []);
  const renderLabel = useCallback(value => <Label text={value} />, []);
  const renderNotch = useCallback(() => <Notch />, []);

  const allLabel = !hideLabel && {
    renderLabel,
    renderNotch,
  };

  const handleValueChange = useCallback((low, high) => {
    const newlow = low.toFixed(1);
    const newhigh = high.toFixed(1);
    setLow(newlow);
    setHigh(newhigh);
    onChange(newlow, newhigh);
  }, []);

  return (
    <RangeSlider
      style={{}}
      min={min}
      max={max}
      step={inputStep}
      low={low}
      high={high}
      floatingLabel={floatingLabel}
      disableRange={rangeDisabled}
      renderThumb={renderThumb}
      renderRail={renderRail}
      renderRailSelected={renderRailSelected}
      {...allLabel}
      onValueChanged={handleValueChange}
    />
  );
};

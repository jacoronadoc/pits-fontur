import React, {useState} from 'react';
import {View, Switch, Text} from 'react-native';

// Styles
import theme from '../Styles/_main';

export default ({text, name, value, onChange}) => {
  const {palette, flexWrap, safeIn} = theme;
  const [checked, setChecked] = useState(value);
  let textIconColor = {
    color: checked ? palette.primary : palette.inverse3,
    paddingLeft: 10,
    paddingTop: 5,
  };

  const onPressAction = isActive => {
    setChecked(isActive);
    onChange({[name]: isActive});
  };

  return (
    <View
      style={{
        ...flexWrap,
        ...safeIn.SmY,
      }}>
      <Switch
        trackColor={{false: palette.inverse3, true: palette.primary}}
        thumbColor={checked ? palette.inverse1 : palette.inverse1}
        ios_backgroundColor={palette.inverse3}
        onValueChange={() => onPressAction(!checked)}
        value={checked}
      />
      <Text style={textIconColor}>{text}</Text>
    </View>
  );
};

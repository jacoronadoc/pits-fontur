import React, {useState} from 'react';
import {TouchableOpacity, Text, View, Image} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Styles
import theme from '../Styles/_main';
import styles from './ButtonIcon.styles';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default ({text, name, icon, image, value, onChange, parentStyle}) => {
  const {palette, buttonSwitchOff, buttonSwitchOn} = theme;
  const [checked, setChecked] = useState(value);
  let styleActive = checked ? buttonSwitchOn : buttonSwitchOff;
  let textIconColor = {
    color: checked ? palette.primary : palette.inverse3,
  };

  const onPressAction = isActive => {
    setChecked(isActive);
    onChange({[name]: isActive});
  };

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={() => onPressAction(!checked)}
      style={{
        ...parentStyle,
        ...styleActive,
        ...styles.buttonIcon,
      }}>
      {image && <Image source={image} style={styles.image} />}
      {icon && <Icon name={icon} size={25} color={textIconColor.color} />}
      <View style={styles.buttonIconTextContainer}>
        <Text style={{...textIconColor, ...theme.safeOut.SmX}}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

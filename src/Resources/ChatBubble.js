import React, {useState, useEffect, useRef} from 'react';
import {View, Image, Text, StyleSheet, Animated} from 'react-native';

// Strings
import theme from '../Styles/_main';

export default ({message, children, minHeight}) => {
  const [height, setHeight] = useState(500);
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const marginAnimation = useRef(new Animated.Value(100)).current;

  const onTextLayout = e => {
    if (height === 500) {
      const newHeight = 15 * e.nativeEvent.lines.length;
      setHeight(newHeight > 15 ? minHeight + newHeight : minHeight);
    }
  };

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
    }).start();
    Animated.timing(marginAnimation, {
      toValue: 0,
      duration: 600,
    }).start();
  }, [height]);

  return (
    <Animated.View
      style={[
        styles.chatBubbleItem,
        {height},
        {
          // Bind opacity to animated value
          opacity: fadeAnim,
          marginTop: marginAnimation,
        },
      ]}>
      <View style={styles.chatBubble}>
        <View style={styles.chatBubbleImgContainer}>
          <Image
            resizeMode="contain"
            style={styles.chatBubbleImg}
            source={require('../assets/images/chat-avatar.png')}
          />
          <View style={styles.triangle} />
        </View>
        <View style={styles.chatBubbleTextContainer}>
          <Text
            style={styles.chatBubbleText}
            numberOfLines={15}
            onTextLayout={onTextLayout}>
            {message}
          </Text>
          {children && <View style={styles.childreContainer}>{children}</View>}
        </View>
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  chatBubbleItem: {
    marginTop: 0,
    marginBottom: 10,
  },
  chatBubble: {
    ...theme.flexRow,
    height: 'auto',
  },
  chatBubbleImg: {
    borderRadius: 25,
    width: 50,
    height: 50,
  },
  chatBubbleTextContainer: {
    flex: 1,
    backgroundColor: theme.palette.default,
    padding: 10,
    height: 'auto',
    borderRadius: 6,
    marginLeft: 10,
    ...theme.palette.shadow,
  },
  chatBubbleText: {
    height: 'auto',
    fontSize: 14.7,
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 10,
    borderTopWidth: 7,
    borderBottomWidth: 7,
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
    borderRightColor: 'white',
    position: 'absolute',
    right: -11,
    top: 20,
    zIndex: 99,
  },
  childreContainer: {
    padding: 10,
    position: 'relative',
  },
});

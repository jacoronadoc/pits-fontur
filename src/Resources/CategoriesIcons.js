import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Styles
import theme from '../Styles/_main';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Categories Icons
const categories = {
  '01': {
    icon: 'nature',
    color: 'rgb(109, 164, 81)',
  },
  '02': {
    icon: 'camera',
    color: 'rgb(141, 7, 78)',
  },
  '03': {
    icon: 'heritage-towns',
    color: 'rgb(95, 38, 114)',
  },
  '04': {
    icon: 'beach',
    color: 'rgb(97, 163, 226)',
  },
  '05': {
    icon: 'archaeological',
    color: 'rgb(173, 148, 40)',
  },
  '06': {
    icon: 'church',
    color: 'rgb(135, 117, 34)',
  },
  '07': {
    icon: 'theme-park',
    color: 'rgb(118, 208, 192)',
  },
  '08': {
    image: require('../assets/images/lgbti-icon.png'),
    color: 'rgb(169, 7, 83)',
  },
  '09': {
    icon: 'natural-parks',
    color: 'rgb(74, 128, 38)',
  },
  10: {
    icon: 'lovely-towns',
    color: 'rgb(138, 39, 156)',
  },
};

export default ({allCategories, mainCategory}) => {
  if (!allCategories && !mainCategory) {
    return false;
  }
  const categoriesArray = allCategories || [mainCategory];
  return (
    <View style={theme.flexWrap}>
      {categoriesArray.map((keyCode, k) => {
        const item = categories[keyCode];
        const ppal =
          keyCode === mainCategory ? {backgroundColor: item.color} : {};
        return (
          <View key={k} style={theme.add(styles.iconContainer, ppal)}>
            {item.image ? (
              <Image key={k} source={item.image} style={styles.image} />
            ) : (
              <Icon key={k} name={item.icon} style={styles.icon} size={12} />
            )}
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  iconContainer: {
    backgroundColor: theme.palette.inverse3,
    borderRadius: 20,
    marginRight: 2,
    padding: 5,
  },
  icon: {
    color: theme.palette.default,
  },
  image: {
    height: 12,
    width: 12,
  },
});

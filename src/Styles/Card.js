export default palette => {
  return {
    container: {
      flex: 1,
      overflow: 'hidden',
      alignItems: 'center',
      justifyContent: 'flex-start',
      backgroundColor: palette.default,
      flexDirection: 'row',
    },
    shadow: {
      borderRadius: 5,
      paddingVertical: 5,
      paddingHorizontal: 5,
      ...palette.shadow,
    },
    favoriteButton: {
      backgroundColor: palette.default,
      borderRadius: 20,
      position: 'absolute',
      zIndex: 3,
      bottom: 10,
      left: 10,
      height: 40,
      width: 40,
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      ...palette.shadow,
    },
    image: {
      width: '100%',
      height: '100%',
      backgroundColor: palette.inverse2,
    },
    infoContainer: {
      flex: 2,
      paddingHorizontal: 5,
    },
    infoLabel: {
      color: palette.primary,
      fontSize: 14,
      fontWeight: 'normal',
      marginBottom: 5,
    },
    infoTitle: {
      color: palette.primary,
      fontSize: 16,
      fontWeight: 'bold',
      marginBottom: 5,
    },
    infoText: {
      color: palette.inverse4,
      fontSize: 12,
      fontWeight: 'normal',
      marginBottom: 5,
    },
    detailsBtn: {
      backgroundColor: palette.inverse2,
      paddingHorizontal: 15,
      paddingVertical: 3,
      borderRadius: 5,
      minHeight: 25,
    },
    footer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    footerIcons: {
      flex: 1,
      flexWrap: 'wrap',
      alignItems: 'flex-start',
      flexDirection: 'row',
      backgroundColor: palette.inverse4,
    },
  };
};

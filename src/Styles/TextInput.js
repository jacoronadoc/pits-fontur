export default pallete => {
  return {
    container: {
      width: '100%',
      marginBottom: 20,
      paddingHorizontal: 18,
    },
    invalid: {
      height: 47,
      borderWidth: 1,
      backgroundColor: pallete.default,
      borderColor: pallete.alert,
      borderRadius: 8,
      opacity: 0.7,
      paddingHorizontal: 31,
      fontSize: 13,
      color: pallete.alert,
      ...pallete.shadow,
    },
    input: {
      height: 47,
      borderWidth: 1,
      backgroundColor: pallete.default,
      borderColor: pallete.default,
      borderRadius: 8,
      opacity: 0.7,
      paddingHorizontal: 31,
      fontSize: 13,
      color: '#38464F',
      ...pallete.shadow,
    },
    placeholder: pallete.inverse4,
  };
};

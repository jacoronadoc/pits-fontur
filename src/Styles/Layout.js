export default {
  flexWrap: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  flexRow: {
    flex: 1,
    flexDirection: 'row',
  },
  flexSpaceBetween: {
    justifyContent: 'space-between',
  },
};

import Typography from './Typography';
import Layout from './Layout';
import Spacing from './Spacing';
import Button from './Button';
import Card from './Card';
import HeaderScroll from './HeaderScrollView';
import textInput from './TextInput';

const palette = {
  default: 'white',
  inverse: 'black',
  inverse1: '#f2f2f2',
  inverse2: '#E5E5E5',
  inverse3: '#c1c1c1',
  inverse4: '#666666',
  primary: '#641D83',
  primary1: '#51177F',
  primary2: 'rgb(170,135,192)',
  primary3: 'rgb(234,225,240)',
  secondary: '#8300D1',
  secondary1: '#731980',
  success: '#8300D1',
  info: '#AC5CFF',
  alert: '#E10053',
  spacing: 35,
  spacingSm: 6,
  spacingMd: 15,
  spacingLg: 25,
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 3.9,
    elevation: 6,
  },
};

export default {
  palette,
  Card: Card(palette),
  HeaderScroll: HeaderScroll(palette),
  ...Typography(palette),
  ...Layout,
  ...Spacing(palette),
  ...Button(palette),
  textInput: textInput(palette),
  add: (...Args) => Args, // theme.add(theme.button, theme.buttonPrimary)
};

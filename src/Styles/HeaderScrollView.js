export default palette => {
  return {
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: palette.inverse2,
    },
    heroContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',
      maxHeight: 100,
      paddingTop: 50,
      paddingHorizontal: 25,
      backgroundColor: palette.default,
    },
    shadow: {
      ...palette.shadow,
      zIndex: 2,
    },
    scrollView: {
      flex: 1,
      width: '100%',
    },
    scrollContainerStyle: {
      justifyContent: 'center',
    },
    contentWrapper: {
      flex: 1,
      backgroundColor: palette.default,
      width: '100%',
      paddingHorizontal: 25,
      paddingTop: 5,
      paddingBottom: 25,
    },
    infoDescription: {
      flex: 1,
      flexDirection: 'row',
      marginVertical: 15,
    },
    infoIconContainer: {
      width: 40,
      height: 40,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 30,
      backgroundColor: palette.primary3,
    },
    infoTextContainer: {
      flex: 1,
      justifyContent: 'center',
      marginLeft: 10,
    },
    infoText: {
      color: palette.inverse3,
      fontSize: 20,
      height: 'auto',
    },
  };
};

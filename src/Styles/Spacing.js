export default palette => {
  const {spacing, spacingSm, spacingMd} = palette;

  return {
    safeIn: {
      SmX: {
        paddingHorizontal: spacingSm,
      },
      SmY: {
        paddingVertical: spacingSm,
      },
      MdX: {
        paddingHorizontal: spacingMd,
      },
      MdY: {
        paddingVertical: spacingSm,
      },
      X: {
        paddingHorizontal: spacing,
      },
      Y: {
        paddingVertical: spacing,
      },
    },
    safeOut: {
      SmX: {
        marginHorizontal: spacingSm,
      },
      SmY: {
        marginVertical: spacingSm,
      },
      MdX: {
        marginHorizontal: spacingMd,
      },
      MdY: {
        marginVertical: spacingMd,
      },
      X: {
        marginHorizontal: spacing,
      },
      Y: {
        marginVertical: spacing,
      },
    },
  };
};

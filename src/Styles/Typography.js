export default palette => {
  const {primary, inverse4, spacingSm, spacingMd} = palette;

  return {
    h2: {
      fontSize: 27,
      color: primary,
      fontWeight: 'bold',
      marginBottom: spacingSm,
    },
    h3: {
      fontSize: 20,
      color: primary,
      fontWeight: 'bold',
      paddingVertical: spacingMd,
    },
    h4: {
      fontSize: 18,
      color: primary,
      fontWeight: 'bold',
      paddingVertical: spacingMd,
    },
    h5: {
      fontSize: 16,
      color: primary,
      fontWeight: 'bold',
    },
    textPrimary: {
      color: primary,
    },
    titleSecondary: {
      color: inverse4,
      fontSize: 20,
      marginBottom: spacingSm,
    },
    titleTertiary: {
      color: inverse4,
      fontSize: 14,
      fontWeight: 'bold',
      marginBottom: 5,
    },
    paragraph: {
      color: inverse4,
      fontSize: 16,
      marginBottom: 5,
    },
  };
};

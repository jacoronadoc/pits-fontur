export default palette => {
  return {
    button: {
      height: 40,
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 10,
      paddingVertical: 2,
      backgroundColor: palette.default,
      borderRadius: 8,
    },
    buttonSm: {
      height: 25,
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 10,
      paddingVertical: 2,
      backgroundColor: palette.default,
      borderRadius: 4,
    },
    buttonText: {
      color: palette.inverse4,
    },
    buttonPrimary: {
      backgroundColor: palette.primary,
    },
    buttonTextPrimary: {
      color: palette.default,
    },
    buttonSwitchOn: {
      borderWidth: 2,
      borderColor: palette.primary,
    },
    buttonSwitchOff: {
      borderWidth: 2,
      borderColor: palette.inverse3,
    },
    bottomLink: {
      padding: 5,
    },
    bottomLinkText: {
      color: palette.inverse4,
      opacity: 0.5,
    },
  };
};

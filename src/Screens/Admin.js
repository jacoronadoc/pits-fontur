import React, {useContext} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';

// Libraries
import LinearGradient from 'react-native-linear-gradient';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Context
import {AppContext} from '../../AppContext';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';
import {version} from '../../package.json';
import {config} from '../Services/ConfigConstants';

const lang = i18n.t('admin');
const langSignInUp = i18n.t('signInUp');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default ({navigation}) => {
  const {appData, handleAppData} = useContext(AppContext);

  return (
    <View style={styles.container}>
      <View style={styles.containerWrapper}>
        <LinearGradient
          colors={[theme.palette.primary1, theme.palette.secondary1]}
          style={styles.heroContainer}>
          <Icon name={'logo'} size={70} color={theme.palette.default} />
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.closeBtn}
            onPress={() => navigation.goBack()}>
            <Icon name={'close'} size={20} color={theme.palette.default} />
          </TouchableOpacity>
        </LinearGradient>
        <View style={theme.add(styles.cardContainer, styles.cardContainerTop)}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.optionsBtn}
            onPress={() => navigation.navigate('Help')}>
            <View style={styles.optionsBtnContainer}>
              <Text>{lang.help}</Text>
              <Icon
                name={'arrow-right'}
                size={10}
                color={theme.palette.inverse4}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.optionsBtn}
            onPress={() =>
              Linking.openURL('https://fontur.com.co/es/preguntas-frecuentes')
            }>
            <View style={styles.optionsBtnContainer}>
              <Text>{lang.faqs}</Text>
              <Icon
                name={'arrow-right'}
                size={10}
                color={theme.palette.inverse4}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.optionsBtn}
            onPress={() => Linking.openURL(langSignInUp.privacyPolicyLink)}>
            <View style={styles.optionsBtnContainer}>
              <Text>{langSignInUp.privacyPolicy}</Text>
              <Icon
                name={'arrow-right'}
                size={10}
                color={theme.palette.inverse4}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.cardContainer}>
          <Text style={theme.h4}>{lang.contactUs}</Text>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.optionsBtn}
            onPress={() => Linking.openURL('mailto:soporte@pits.gov.co')}>
            <View style={styles.optionsBtnContainer}>
              <Text>soporte@pits.gov.co</Text>
              <Icon
                name={'arrow-right'}
                size={10}
                color={theme.palette.inverse4}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.optionsBtn}
            onPress={() => Linking.openURL('tel:573212345634')}>
            <View style={styles.optionsBtnContainer}>
              <Text>+57 321 234 5634</Text>
              <Icon
                name={'arrow-right'}
                size={10}
                color={theme.palette.inverse4}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.optionsBtn}
            onPress={() => Linking.openURL('tel:6012343456')}>
            <View style={styles.optionsBtnContainer}>
              <Text>601 234 3456</Text>
              <Icon
                name={'arrow-right'}
                size={10}
                color={theme.palette.inverse4}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={theme.add(styles.cardContainer, styles.cardContainerBottom)}>
        <View style={styles.socialButtonsContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.socialBtn}
            onPress={() => Linking.openURL('https://twitter.com/fonturcol')}>
            <Icon name={'social-tw'} size={20} color={theme.palette.default} />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.socialBtn}
            onPress={() =>
              Linking.openURL('https://www.facebook.com/FONTURcolombia/')
            }>
            <Icon name={'social-fb'} size={20} color={theme.palette.default} />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.socialBtn}
            onPress={() =>
              Linking.openURL('https://www.instagram.com/fonturcol/')
            }>
            <Icon name={'social-ig'} size={20} color={theme.palette.default} />
          </TouchableOpacity>
        </View>
        <Text style={styles.versionTxt}>{`${lang.version}: ${version}`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.inverse2,
  },
  containerWrapper: {
    flex: 1,
    width: '100%',
  },
  heroContainer: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    alignItems: 'center',
    width: '100%',
    maxHeight: 160,
    paddingTop: 60,
    paddingHorizontal: 25,
  },
  cardContainer: {
    backgroundColor: theme.palette.default,
    width: '100%',
    paddingHorizontal: 25,
    paddingVertical: 15,
    ...theme.palette.shadow,
    marginVertical: 8,
    minHeight: 130,
  },
  cardContainerTop: {
    marginVertical: 0,
    marginBottom: 8,
  },
  cardContainerBottom: {
    marginVertical: 0,
    paddingBottom: 40,
  },
  optionsBtn: {
    width: '100%',
    height: 40,
    borderColor: theme.palette.inverse2,
    borderBottomWidth: 1,
  },
  optionsBtnContainer: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    alignItems: 'center',
  },
  socialButtonsContainer: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: 50,
    marginBottom: 10,
  },
  socialBtn: {
    flex: 1,
    maxWidth: 40,
    height: 40,
    backgroundColor: theme.palette.primary,
    marginHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  versionTxt: {
    textAlign: 'center',
  },
});

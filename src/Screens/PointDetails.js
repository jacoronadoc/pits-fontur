import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  useWindowDimensions,
  ImageBackground,
  Linking,
} from 'react-native';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Methods
import {
  getFavorites,
  setFavorite,
  deleteFavorite,
} from '../Components/PitsMap.handleFavorites';

// Components
import Carousel from '../Components/Carousel';

//Resources
import ButtonStars from '../Resources/ButtonStars';
import CategoriesIcons from '../Resources/CategoriesIcons';

// Services
import {stampToHour} from '../Services/dateServices';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('pointDetails');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Context
import {AppContext} from '../../AppContext';

// Services
import {checkFavorite} from '../Services/checkFavorite';
import {getYoutubeId} from '../Services/getQuerystring';

export default ({route, navigation}) => {
  const {HeaderScroll} = theme;
  const {appData, handleAppData} = useContext(AppContext);
  const {point} = route.params;
  const {width: windowWidth} = useWindowDimensions();
  const [headerDetails, setHeaderDetails] = useState(false);
  const [isFavorite, setIsFavorite] = useState(undefined);
  const [images, setImages] = useState([]);
  const [showImages, setShowImages] = useState(true);
  const [videos, setVideos] = useState([]);
  const [showVideos, setShowVideos] = useState(false);
  const [images360, setImages360] = useState([]);
  const [showImages360, setShowImages360] = useState(false);

  const shadowStyle = headerDetails ? HeaderScroll.shadow : null;

  const favoriteAction = async (item, itemChecked) => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      const result = !itemChecked
        ? await setFavorite(item, appData.user.accessToken)
        : await deleteFavorite(itemChecked.id, appData.user.accessToken);
      if (result) {
        const favoritesList = await getFavorites(appData.user.accessToken);
        if (favoritesList) {
          handleAppData({
            ...appData,
            favorites: favoritesList,
          });
        }
      }
    }
  };

  const handleScroll = event => {
    if (event.nativeEvent.contentOffset.y > 50) {
      setHeaderDetails(true);
    } else {
      setHeaderDetails(false);
    }
  };

  useEffect(() => {
    const newFavorite = checkFavorite(appData.favorites, point);
    if (newFavorite !== isFavorite) {
      setIsFavorite(newFavorite);
    }
  }, [appData.favorites]);

  useEffect(() => {
    if (point.multimedia.length > 0) {
      const newImages = [];
      const newVideos = [];
      const newImages360 = [];

      point.multimedia.map(item => {
        if (item.id_multimedia.indexOf('foto_') > -1) {
          newImages.push(item);
        } else if (item.id_multimedia.indexOf('video_') > -1) {
          item.videoId = getYoutubeId(item.url);
          item.thumb = `https://img.youtube.com/vi/${item.videoId}/mqdefault.jpg`;
          newVideos.push(item);
        } else if (item.id_multimedia.indexOf('foto360_') > -1) {
          newImages360.push(item);
        }
      });

      if (newImages.length > 0) {
        setImages(newImages);
      }

      if (newVideos.length > 0) {
        setVideos(newVideos);
        if (newImages.length === 0) {
          setShowVideos(true);
          setShowImages(false);
          setShowImages360(false);
        }
      }

      if (newImages360.length > 0) {
        setImages360(newImages360);

        if (newImages.length === 0 && newVideos.length === 0) {
          setShowVideos(false);
          setShowImages(false);
          setShowImages360(true);
        }
      }
    }
  }, [point.multimedia]);

  return (
    <View style={HeaderScroll.container}>
      <View style={theme.add(styles.heroContainer, shadowStyle)}>
        <Text style={styles.headerTitle} numberOfLines={1}>
          {headerDetails ? point.title : ''}
        </Text>
        <View style={styles.headerOptions}>
          {isFavorite ? (
            <TouchableOpacity
              onPress={() => favoriteAction(point, isFavorite)}
              style={styles.headerBtn}>
              <Icon name={'heart'} size={20} color={theme.palette.secondary} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => favoriteAction(point, undefined)}
              style={styles.headerBtn}>
              <Icon
                name={'heart-outline'}
                size={20}
                color={theme.palette.secondary}
              />
            </TouchableOpacity>
          )}
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.headerBtn}
            onPress={() => navigation.goBack()}>
            <Icon name={'close'} size={20} color={theme.palette.primary} />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView
        style={HeaderScroll.scrollView}
        onScroll={handleScroll}
        scrollEventThrottle={1}
        contentContainerStyle={HeaderScroll.scrollContainerStyle}>
        <View style={HeaderScroll.contentWrapper}>
          <Text style={styles.title}>{point.title}</Text>
          <Text
            style={
              theme.titleSecondary
            }>{`${point.nombre_depto}, ${point.nombre_mpio}`}</Text>
          {images.length > 0 && showImages && (
            <View style={styles.carouselContent}>
              <Carousel
                dots={true}
                pages={images}
                frameWidth={windowWidth - 50}>
                {images.map((image, index) => (
                  <ImageBackground
                    key={index}
                    source={{uri: image.url}}
                    style={theme.add(styles.image, {width: windowWidth - 50})}
                  />
                ))}
              </Carousel>
            </View>
          )}
          {videos.length > 0 && showVideos && (
            <View style={styles.carouselContent}>
              <Carousel
                dots={true}
                pages={videos}
                frameWidth={windowWidth - 50}>
                {videos.map((image, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() =>
                      navigation.navigate('VideoWebView', {
                        videoId: image.videoId,
                      })
                    }
                    activeOpacity={1}>
                    <View style={styles.actionCircle}>
                      <View style={styles.triangle} />
                    </View>
                    <ImageBackground
                      source={{uri: image.thumb}}
                      style={theme.add(styles.image, {width: windowWidth - 50})}
                    />
                  </TouchableOpacity>
                ))}
              </Carousel>
            </View>
          )}
          {images360.length > 0 && showImages360 && (
            <View style={styles.carouselContent}>
              <Carousel
                dots={true}
                pages={images360}
                frameWidth={windowWidth - 50}>
                {images360.map((image, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() =>
                      navigation.navigate('Image360WebView', {
                        image: image.url,
                      })
                    }
                    activeOpacity={1}>
                    <View style={styles.actionCircle}>
                      <Icon
                        name={'explore-360'}
                        size={35}
                        color={theme.palette.default}
                      />
                    </View>
                    <ImageBackground
                      source={{uri: image.url}}
                      style={theme.add(styles.image, {width: windowWidth - 50})}
                    />
                  </TouchableOpacity>
                ))}
              </Carousel>
            </View>
          )}
          <View style={styles.geleryBtnContainer}>
            {(videos.length > 0 || images360.length > 0) &&
              images.length > 0 &&
              !showImages && (
                <TouchableOpacity
                  style={styles.btnAction}
                  onPress={() => {
                    setShowVideos(false);
                    setShowImages(true);
                    setShowImages360(false);
                  }}>
                  <Icon
                    name={'camera'}
                    size={18}
                    color={theme.palette.primary}
                  />
                  <Text style={styles.btnActionText}>{lang.photos}</Text>
                </TouchableOpacity>
              )}
            {(images.length > 0 || images360.length > 0) &&
              videos.length > 0 &&
              !showVideos && (
                <TouchableOpacity
                  style={styles.btnAction}
                  onPress={() => {
                    setShowVideos(true);
                    setShowImages(false);
                    setShowImages360(false);
                  }}>
                  <Icon
                    name={'video'}
                    size={14}
                    color={theme.palette.primary}
                  />
                  <Text style={styles.btnActionText}>{lang.videos}</Text>
                </TouchableOpacity>
              )}
            {(videos.length > 0 || images.length > 0) &&
              images360.length > 0 &&
              !showImages360 && (
                <TouchableOpacity
                  style={styles.btnAction}
                  onPress={() => {
                    setShowVideos(false);
                    setShowImages(false);
                    setShowImages360(true);
                  }}>
                  <Icon
                    name={'explore-360'}
                    size={18}
                    color={theme.palette.primary}
                  />
                  <Text style={styles.btnActionText}>{lang.images360}</Text>
                </TouchableOpacity>
              )}
          </View>
          <View style={styles.content}>
            <Text style={theme.paragraph}>{point.description}</Text>
          </View>
          {(point.allCategories || point.categoria_principal) && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.typeTourism}</Text>
              <CategoriesIcons
                allCategories={point.allCategories}
                mainCategory={point.categoria_principal}
              />
            </View>
          )}
          {point.apertura && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.openingHours}</Text>
              <Text style={theme.paragraph}>
                {`${stampToHour(point.apertura)} - ${stampToHour(
                  point.cierre,
                )}`}
              </Text>
            </View>
          )}
          {point.direccion && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.address}</Text>
              <Text style={theme.paragraph}>{point.direccion}</Text>
            </View>
          )}
          {point.tiempo_recorrido && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.travelTime}</Text>
              <Text style={theme.paragraph}>
                {`${point.tiempo_recorrido} ${
                  point.tiempo_recorrido > 1 ? lang.hours : lang.hour
                }`}
              </Text>
            </View>
          )}
          {point.obs_presupuesto && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.price}</Text>
              <Text style={theme.paragraph}>{point.obs_presupuesto}</Text>
            </View>
          )}
          {point.temperatura && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.wheater}</Text>
              <Text style={theme.paragraph}>{point.temperatura}</Text>
            </View>
          )}
          {point.tipo_boleteria && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>
                {`${lang.tickets}: ${point.tipo_boleteria}`}
              </Text>
              {point.adquisicion_boleteria && (
                <Text style={theme.paragraph}>
                  {point.adquisicion_boleteria}
                </Text>
              )}
            </View>
          )}
          {(point.telefono || point.telefono_secundario || point.web) && (
            <View style={styles.content}>
              <Text style={theme.titleTertiary}>{lang.contact}</Text>
            </View>
          )}
          {point.telefono && (
            <View style={styles.content}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.optionsBtn}
                onPress={() => Linking.openURL(`tel:${point.telefono}`)}>
                <View style={styles.optionsBtnContainer}>
                  <Text numberOfLines={1}>{point.telefono}</Text>
                  <Icon
                    name={'arrow-right'}
                    size={10}
                    color={theme.palette.inverse4}
                  />
                </View>
              </TouchableOpacity>
            </View>
          )}
          {point.telefono_secundario && (
            <View style={styles.content}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.optionsBtn}
                onPress={() =>
                  Linking.openURL(`tel:${point.telefono_secundario}`)
                }>
                <View style={styles.optionsBtnContainer}>
                  <Text numberOfLines={1}>{point.telefono_secundario}</Text>
                  <Icon
                    name={'arrow-right'}
                    size={10}
                    color={theme.palette.inverse4}
                  />
                </View>
              </TouchableOpacity>
            </View>
          )}
          {point.web && (
            <View style={styles.content}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.optionsBtn}
                onPress={() => Linking.openURL(point.web)}>
                <View style={styles.optionsBtnContainer}>
                  <Text numberOfLines={1}>{point.web}</Text>
                  <Icon
                    name={'arrow-right'}
                    size={10}
                    color={theme.palette.inverse4}
                  />
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
  },
  headerTitle: {
    flex: 2,
    ...theme.h4,
  },
  headerOptions: {
    ...theme.flexRow,
    justifyContent: 'space-between',
    maxWidth: 70,
  },
  headerBtn: {
    width: 25,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...theme.h2,
    marginTop: 0,
    marginBottom: 15,
  },
  content: {
    paddingHorizontal: 25,
    paddingBottom: 15,
  },
  carouselContent: {
    paddingVertical: 15,
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 30,
    borderTopWidth: 20,
    borderBottomWidth: 20,
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
    borderLeftColor: 'white',
    left: 5,
  },
  actionCircle: {
    position: 'absolute',
    zIndex: 2,
    top: '50%',
    left: '50%',
    width: 80,
    height: 80,
    backgroundColor: 'rgba(0,0,0,0.6)',
    transform: [{translateX: -40}, {translateY: -40}],
    borderRadius: 40,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainer: {
    width: 300,
    height: 200,
    backgroundColor: theme.palette.inverse2,
  },
  image: {
    width: '100%',
    height: 220,
    backgroundColor: theme.palette.inverse2,
  },
  optionsBtn: {
    width: '100%',
    height: 40,
    borderColor: theme.palette.inverse2,
    borderBottomWidth: 1,
  },
  optionsBtnContainer: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    alignItems: 'center',
  },
  geleryBtnContainer: {
    ...theme.flexRow,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
  },
  btnAction: {
    ...theme.flexRow,
    ...theme.button,
    backgroundColor: theme.palette.primary3,
    marginHorizontal: 3,
    maxWidth: '35%',
  },
  btnActionText: {
    color: theme.palette.primary,
    marginLeft: 5,
  },
});

import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
  Linking,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

// Strings
import {config} from '../Services/ConfigConstants';
import i18n from '../nls/i18n';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

import theme from '../Styles/_main';

const lang = i18n.t('signInUp');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Services
import {getData, postData} from '../Services/dataServices';
import validateEmail from '../Services/validateEmail';

//Resources
import KeyboardAvoidingWrapper from '../Resources/KeyboardAvoidingWrapper';

export default ({navigation}) => {
  const {textInput} = theme;

  const [compiledData, setPostData] = useState({
    role: 'Turista',
  });

  // const dispatch = useDispatch();
  const [unfilteredData, setUnfilteredData] = useState();
  const [filteredData, setFilteredData] = useState([{}]);

  const [name, setName] = useState('');
  const [country, setCountry] = useState();
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState(null);

  const [isEmailValid, setIsEmailValid] = useState(true);
  const [doesPasswordMatch, setDoesPasswordMatch] = useState(false);

  const handleInputChange = (key, value) => {
    const newCompiledData = {
      ...compiledData,
      [key]: value,
    };
    setPostData(newCompiledData);
  };

  useEffect(() => {
    setDoesPasswordMatch(password === passwordConfirmation);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [password, passwordConfirmation]);

  useEffect(() => {
    handleInputChange(
      'password',
      doesPasswordMatch ? passwordConfirmation : null,
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [doesPasswordMatch]);

  useEffect(() => {
    const valid = validateEmail(email);
    handleInputChange('email', valid ? email : null);
    setIsEmailValid(valid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [email]);

  useEffect(() => {
    getData('api/countries')
      .then(response => response)
      .then(data => {
        if (data.length > 0) {
          setUnfilteredData(data);
        }
      });
  }, []);

  useEffect(() => {
    const tmpArray = [];
    for (const key in unfilteredData) {
      const item = unfilteredData[key];
      const label = item[`name_${i18n.currentLocale()}`] || item.name_es;
      tmpArray.push({
        label,
        value: item.iso,
        key: item.iso,
      });
    }
    setFilteredData(tmpArray);
  }, [unfilteredData]);

  const selected = {
    inputAndroid: textInput.input,
    inputIOS: textInput.input,
    placeholder: textInput.placeholder,
  };

  const handleSignUp = async () => {
    const response = await postData('api/auth/signup', compiledData);
    if (response.status === 200) {
      navigation.navigate('SignUpConfirmation', {
        title: lang.signUpSuccessTitle,
        description: lang.signUpSuccessMessage,
        backTo: 'Sign Up',
      });
    } else {
      Alert.alert(lang.signUpFailedTitle, lang.signUpFailedMessage, [
        {text: lang.signUpSuccessOk, onPress: () => false},
      ]);
    }
  };

  return (
    <KeyboardAvoidingWrapper>
      <View style={styles.container}>
        <View style={styles.containerWrapper}>
          <View style={theme.add(textInput.container, {alignItems: 'center'})}>
            <Icon name={'logo'} size={120} color={theme.palette.primary} />
          </View>
          <Text style={styles.title}>{lang.createAccount}</Text>
          <View style={textInput.container}>
            <TextInput
              placeholderTextColor={textInput.placeholder}
              style={textInput.input}
              placeholder={lang.inputName}
              value={name}
              onChangeText={txt => {
                handleInputChange('name', txt);
                setName(txt);
              }}
            />
          </View>
          <View style={textInput.container}>
            <RNPickerSelect
              onValueChange={txt => {
                handleInputChange('country', txt);
                setCountry(txt);
              }}
              useNativeAndroidPickerStyle={false}
              items={filteredData}
              value={country}
              placeholder={{
                label: lang.inputCountry,
                value: '',
              }}
              style={selected}
            />
          </View>
          <View style={textInput.container}>
            <TextInput
              placeholderTextColor={textInput.placeholder}
              style={textInput.input}
              placeholder={lang.inputPhone}
              value={phone}
              keyboardType="numeric"
              onChangeText={txt => {
                handleInputChange('phone', txt.replace(/[^0-9]/g, ''));
                setPhone(txt.replace(/[^0-9]/g, ''));
              }}
            />
          </View>
          <View style={textInput.container}>
            <TextInput
              placeholderTextColor={textInput.placeholder}
              style={
                email !== null
                  ? isEmailValid
                    ? textInput.input
                    : textInput.invalid
                  : textInput.input
              }
              placeholder={lang.inputEmail}
              value={email}
              onChangeText={txt => {
                setEmail(txt);
              }}
            />
          </View>
          <View style={textInput.container}>
            <TextInput
              secureTextEntry={true}
              placeholderTextColor={textInput.placeholder}
              style={textInput.input}
              placeholder={lang.inputPassword}
              value={password}
              onChangeText={txt => {
                setPassword(txt);
              }}
            />
          </View>
          <View style={textInput.container}>
            <TextInput
              secureTextEntry={true}
              placeholderTextColor={textInput.placeholder}
              style={
                passwordConfirmation !== null
                  ? doesPasswordMatch
                    ? textInput.input
                    : textInput.invalid
                  : textInput.input
              }
              placeholder={lang.inputPasswordConfirm}
              value={passwordConfirmation}
              onChangeText={txt => {
                setPasswordConfirmation(txt);
              }}
            />
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={handleSignUp}
            style={theme.add(theme.button, theme.buttonPrimary)}>
            <Text style={theme.buttonTextPrimary}>{lang.buttonSignUp}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rowSpaceBetween}>
          <View>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={theme.bottomLink}>
              <Text style={theme.bottomLinkText}>{`‹ ${lang.back}`}</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => Linking.openURL(lang.privacyPolicyLink)}
              style={theme.bottomLink}>
              <Text style={theme.bottomLinkText}>{lang.privacyPolicy}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAvoidingWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.inverse2,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2,
  },
  title: {
    ...theme.h2,
    marginBottom: 30,
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
});

import React from 'react';
import {View, StyleSheet, Image, Pressable, Text, Linking} from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';

// Styles
import theme from '../Styles/_main';
import i18n from '../nls/i18n';

const lang = i18n.t('signInUp');

//https://www.npmjs.com/package/react-native-onboarding-swiper
export default ({navigation}) => {
  const Square = ({isLight, selected}) => {
    let backgroundColor;
    if (isLight) {
      backgroundColor = selected
        ? theme.palette.primary
        : theme.palette.default;
    } else {
      backgroundColor = selected ? '#fff' : 'rgba(255, 255, 255, 0.5)';
    }
    return <View style={{...styles.dots, ...{backgroundColor}}} />;
  };

  return (
    <View style={styles.container}>
      <View style={styles.container}>
        <Onboarding
          showSkip={true}
          onSkip={() => navigation.navigate('Sign In Options')}
          onDone={() => {
            navigation.navigate('Sign In Options');
          }}
          bottomBarColor={'white'}
          DotComponent={Square}
          pages={[
            {
              backgroundColor: '#fff',
              image: <Image source={require('../assets/images/splash1.png')} />,
              title: '',
              subtitle: '',
            },
            {
              backgroundColor: '#fff',
              image: <Image source={require('../assets/images/splash2.png')} />,
              title: '',
              subtitle: '',
            },
            {
              backgroundColor: '#fff',
              image: <Image source={require('../assets/images/splash3.png')} />,
              title: '',
              subtitle: '',
            },
          ]}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Pressable
          style={styles.buttonPrimary}
          onPress={() => Linking.openURL(lang.privacyPolicyLink)}>
          <Text style={styles.textBtn}>{lang.privacyPolicy}</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  dots: {
    width: 14,
    height: 14,
    marginHorizontal: 8,
    borderRadius: 10,
    borderColor: theme.palette.success,
    borderWidth: 1,
  },
  buttonContainer: {
    alignItems: 'center',
    bottom: 100,
    flex: 1,
    position: 'absolute',
    width: '100%',
  },
  buttonPrimary: {
    ...theme.button,
    ...theme.buttonPrimary,
    width: 250,
  },
  textBtn: {
    color: theme.palette.default,
    fontSize: 16,
  },
});

import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('signInUp');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

//Resources
import KeyboardAvoidingWrapper from '../Resources/KeyboardAvoidingWrapper';

export default ({route, navigation}) => {
  const {title, description, backTo} = route.params;
  return (
    <KeyboardAvoidingWrapper>
      <View style={styles.container}>
        <View style={styles.containerWrapper}>
          <View style={styles.textContainer}>
            <Icon name={'logo'} size={120} color={theme.palette.primary} />
          </View>
          <Image source={require('../assets/images/check-in.png')} />
          <View style={styles.textContainer}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.desc}>{description}</Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => navigation.navigate(backTo)}
            style={theme.add(theme.button, theme.buttonPrimary)}>
            <Text style={theme.buttonTextPrimary}>{lang.signUpSuccessOk}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rowSpaceBetween}>
          <View>
            <TouchableOpacity
              onPress={() => navigation.navigate(backTo)}
              style={theme.bottomLink}>
              <Text style={theme.bottomLinkText}>{`‹ ${lang.main}`}</Text>
            </TouchableOpacity>
          </View>
          <View />
        </View>
      </View>
    </KeyboardAvoidingWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.inverse2,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    marginTop: 15,
    marginBottom: 30,
    paddingHorizontal: 25,
  },
  title: {
    ...theme.h3,
    marginBottom: 10,
    textAlign: 'center',
  },
  desc: {
    color: theme.palette.primary,
    fontSize: 16,
    textAlign: 'center',
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
});

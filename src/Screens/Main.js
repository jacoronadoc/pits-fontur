import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {TabNavigatorScreenOptions} from './Main.TabNavigator';
import i18n from '../nls/i18n';

// Context
import {AppContext} from '../../AppContext';

// Screens
import PitsMap from '../Components/PitsMap';
import ARScene from '../Components/ARScene';
import MyPits from './MyPits';
import NewsList from './NewsList';

const Tab = createBottomTabNavigator();
const tabs = i18n.t('tabs');

const TabTwo = () => (
  <View style={styles.container}>
    <Text>{i18n.t('comingSoon')}</Text>
  </View>
);

const Main = ({navigation}) => {
  const {appData} = useContext(AppContext);

  useEffect(() => {
    if (appData.firstAccess) {
      navigation.navigate('Landing');
    }
  }, [appData.firstAccess]);

  return (
    <Tab.Navigator screenOptions={TabNavigatorScreenOptions}>
      <Tab.Screen name={tabs.map} component={PitsMap} />
      <Tab.Screen name={tabs.ar} component={ARScene} />
      <Tab.Screen name={tabs.myPits} component={MyPits} />
      <Tab.Screen name={tabs.news} component={NewsList} />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Main;

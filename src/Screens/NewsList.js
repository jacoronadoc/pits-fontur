import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('news');
const locale = i18n.currentLocale();

// Components
import Card from '../Components/Card';

// Services
import {getData} from '../Services/dataServices';

// Context
import {AppContext} from '../../AppContext';

export default ({navigation}) => {
  const {appData} = useContext(AppContext);
  const {HeaderScroll} = theme;
  const [headerDetails, setHeaderDetails] = useState(false);
  const [newsList, setNewsList] = useState([]);
  const [noNews, setNoNews] = useState(false);
  const shadowStyle = headerDetails ? HeaderScroll.shadow : null;

  const handleScroll = event => {
    if (event.nativeEvent.contentOffset.y > 50) {
      setHeaderDetails(true);
    } else {
      setHeaderDetails(false);
    }
  };

  const accountAction = () => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      return navigation.navigate('Account');
    }
  };

  useEffect(() => {
    (async () => {
      const newsService = await getData('api/news/list/all');
      if (newsService && newsService.message.length > 0) {
        setNoNews(false);
        setNewsList(newsService.message);
      } else {
        setNoNews(true);
      }
    })();
  }, []);

  return (
    <View style={styles.container}>
      <View style={theme.add(styles.heroContainer, shadowStyle)}>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => navigation.navigate('Admin')}>
          <Image source={require('../assets/images/icon-info.png')} />
        </TouchableOpacity>
        <Text numberOfLines={1} style={styles.title}>
          {lang.title}
        </Text>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => accountAction()}>
          <Image source={require('../assets/images/icon-account.png')} />
        </TouchableOpacity>
      </View>
      <ScrollView
        style={HeaderScroll.scrollView}
        onScroll={handleScroll}
        scrollEventThrottle={1}
        contentContainerStyle={HeaderScroll.scrollContainerStyle}>
        <View style={styles.contentWrapper}>
          {newsList.map((item, k) => (
            <TouchableOpacity
              key={k}
              style={styles.headerBtn}
              activeOpacity={0.8}
              onPress={() => navigation.navigate('News Details', {item})}>
              <Card
                item={{
                  ...item,
                  title: item[`title_${locale}`],
                  description: item[`description_${locale}`],
                  label: item.layer,
                  image:
                    item.image_path ||
                    'https://picsum.photos/id/1015/300/200.jpg',
                }}
                containerStyle={styles.cardContainer}
                titleLines={2}
                titleStyle={styles.cardTitle}
                descriptionLines={3}
                descriptionStyle={styles.cardDescription}
                infoContainerStyle={styles.cardInfoContainer}
                labelStyle={styles.cardLabel}
                labelTop={true}
                shadow={true}
              />
            </TouchableOpacity>
          ))}
          {noNews && (
            <View style={HeaderScroll.infoDescription}>
              <View style={HeaderScroll.infoIconContainer}>
                <Icon
                  style={HeaderScroll.infoIcon}
                  name="info"
                  size={20}
                  color={theme.palette.primary2}
                />
              </View>
              <View style={HeaderScroll.infoTextContainer}>
                <Text style={HeaderScroll.infoText}>{lang.noNews}</Text>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...theme.HeaderScroll.container,
    backgroundColor: theme.palette.inverse2,
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
    paddingHorizontal: 20,
    paddingBottom: 8,
    backgroundColor: theme.palette.inverse2,
  },
  title: {
    fontSize: 25,
    color: theme.palette.primary,
    fontWeight: 'bold',
    padding: 0,
    marginHorizontal: 5,
    maxWidth: 210,
  },
  contentWrapper: {
    ...theme.HeaderScroll.contentWrapper,
    backgroundColor: 'rgba(255,255,255,0)',
  },
  cardContainer: {
    width: '100%',
    height: 130,
    marginHorizontal: 0,
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  cardDescription: {
    fontSize: 15,
  },
  cardTitle: {
    fontSize: 18,
  },
  cardInfoContainer: {
    paddingHorizontal: 0,
    paddingLeft: 10,
  },
  cardLabel: {
    fontSize: 15,
  },
});

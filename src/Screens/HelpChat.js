import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Linking} from 'react-native';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Resorces
import ChatBubble from '../Resources/ChatBubble';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('help');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default ({route, navigation}) => {
  const {phonesActive} = route.params || false;
  const [showPhones, setShowPhones] = useState(phonesActive);

  return (
    <View style={styles.container}>
      <View style={theme.add(styles.heroContainer)}>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => navigation.goBack()}>
          <Icon name="close" size={20} color={'black'} />
        </TouchableOpacity>
        <Text style={styles.title}>{lang.chatWithUs}</Text>
        <Text>{'   '}</Text>
      </View>
      <View style={styles.containerWrapper}>
        <View style={styles.chatBubblesContainer}>
          {!phonesActive && (
            <ChatBubble message={lang.chatMsj01} minHeight={130}>
              <View>
                <TouchableOpacity
                  style={styles.buttonOptionsV}
                  onPress={() => setShowPhones(true)}>
                  <Text style={theme.add(theme.buttonTextPrimary)}>
                    {lang.phoneLines}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.buttonOptionsV}
                  onPress={() => navigation.navigate('ContactUs')}>
                  <Text style={theme.add(theme.buttonTextPrimary)}>
                    {lang.contactUs}
                  </Text>
                </TouchableOpacity>
              </View>
            </ChatBubble>
          )}
          {showPhones && (
            <ChatBubble message={lang.chatMsj02} minHeight={110}>
              <View>
                <View>
                  <TouchableOpacity
                    style={styles.buttonOptionsV}
                    onPress={() => Linking.openURL('tel:123')}>
                    <Text style={theme.add(theme.buttonTextPrimary)}>
                      {lang.emergency}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={theme.add(theme.flexRow, theme.flexSpaceBetween)}>
                  <TouchableOpacity
                    style={styles.buttonOptionsH}
                    onPress={() => Linking.openURL('tel:112')}>
                    <Text style={theme.add(theme.buttonTextPrimary)}>
                      {lang.police}
                    </Text>
                  </TouchableOpacity>
                  <Text>{'  '}</Text>
                  <TouchableOpacity
                    style={styles.buttonOptionsH}
                    onPress={() => Linking.openURL('tel:125')}>
                    <Text style={theme.add(theme.buttonTextPrimary)}>
                      {lang.hospitals}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ChatBubble>
          )}
        </View>
      </View>
      <View style={styles.rowSpaceBetween}>
        <View>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={theme.bottomLink}>
            <Text style={theme.bottomLinkText}>{`‹ ${lang.back}`}</Text>
          </TouchableOpacity>
        </View>
        <View />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.inverse2,
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
    paddingHorizontal: 20,
    paddingBottom: 8,
    backgroundColor: theme.palette.inverse2,
  },
  title: {
    fontSize: 25,
    color: theme.palette.primary,
    fontWeight: 'bold',
    padding: 0,
    marginHorizontal: 5,
    maxWidth: 210,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 15,
    width: '100%',
  },
  chatBubblesContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },
  buttonOptionsH: {
    flex: 1,
    ...theme.buttonSm,
    ...theme.buttonPrimary,
  },
  buttonOptionsV: {
    ...theme.buttonSm,
    ...theme.buttonPrimary,
    marginVertical: 5,
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
});

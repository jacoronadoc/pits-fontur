import * as React from 'react';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';
import i18n from '../nls/i18n';

const Icon = createIconSetFromIcoMoon(icomoonConfig);
const tabs = i18n.t('tabs');

const TabNavigatorScreenOptions = ({route}) => ({
  tabBarIcon: ({focused, color, size}) => {
    let IconName;
    const icons = ['location', 'explore', 'qr-code', 'pits', 'megaphone'];

    const tabItems = Object.values(tabs);
    const currentTabName = tabItems.filter(v => v === route.name);
    const currentTabIndex = tabItems.indexOf(currentTabName[0]);
    IconName = icons[currentTabIndex];
    // IconName = focused ? 'palenquera' : icons[currentTabIndex];

    return <Icon name={IconName} size={25} color="darkmagenta" />;
  },
  tabBarActiveTintColor: 'darkmagenta',
  tabBarInactiveTintColor: 'darkmagenta',
});

export {TabNavigatorScreenOptions};

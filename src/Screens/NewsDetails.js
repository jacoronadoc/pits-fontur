import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('news');
const locale = i18n.currentLocale();

// Context
import {AppContext} from '../../AppContext';

export default ({route, navigation}) => {
  const {appData} = useContext(AppContext);
  const {item} = route.params;
  const {HeaderScroll} = theme;
  const [headerDetails, setHeaderDetails] = useState(false);
  const shadowStyle = headerDetails ? HeaderScroll.shadow : null;

  const handleScroll = event => {
    if (event.nativeEvent.contentOffset.y > 50) {
      setHeaderDetails(true);
    } else {
      setHeaderDetails(false);
    }
  };

  const accountAction = () => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      return navigation.navigate('Account');
    }
  };

  return (
    <View style={styles.container}>
      <View style={theme.add(styles.heroContainer, shadowStyle)}>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={20} color={theme.palette.primary} />
        </TouchableOpacity>
        <Text numberOfLines={1} style={styles.title}>
          {lang.title}
        </Text>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => accountAction()}>
          <Image source={require('../assets/images/icon-account.png')} />
        </TouchableOpacity>
      </View>
      <ScrollView
        style={HeaderScroll.scrollView}
        onScroll={handleScroll}
        scrollEventThrottle={1}
        contentContainerStyle={HeaderScroll.scrollContainerStyle}>
        <View style={styles.contentWrapper}>
          <View style={styles.contentInner}>
            <Text style={styles.contentLocation}>{item.layer}</Text>
            <Text style={styles.contentTitle}>{item[`title_${locale}`]}</Text>
          </View>
          {item.image_path && (
            <Image style={styles.image} source={{uri: item.image_path}} />
          )}
          <View style={styles.contentInner}>
            <Text style={theme.paragraph}>{item[`content_${locale}`]}</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...theme.HeaderScroll.container,
    backgroundColor: theme.palette.inverse2,
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
    paddingHorizontal: 20,
    paddingBottom: 8,
    backgroundColor: theme.palette.inverse2,
  },
  headerBtn: {
    width: 43,
    height: 43,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 27,
    color: theme.palette.primary,
    fontWeight: 'bold',
    padding: 0,
    marginHorizontal: 5,
    maxWidth: 210,
  },
  contentWrapper: {
    ...theme.HeaderScroll.contentWrapper,
    backgroundColor: 'rgba(255,255,255,0)',
  },
  contentInner: {
    marginHorizontal: 25,
  },
  contentLocation: {
    fontSize: 18,
    color: theme.palette.primary,
    marginBottom: 15,
  },
  contentTitle: {
    ...theme.h2,
    marginBottom: 15,
  },
  image: {
    width: '100%',
    height: 220,
    marginBottom: 20,
  },
  contentText: {
    color: theme.palette.inverse4,
    fontSize: 14,
  },
});

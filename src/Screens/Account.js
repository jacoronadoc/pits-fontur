import React, {useState, useContext} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';

// Libraries
import LinearGradient from 'react-native-linear-gradient';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Context
import {AppContext, AppDataDefault} from '../../AppContext';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('account');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default ({navigation}) => {
  const {appData, handleAppData} = useContext(AppContext);
  const [accordion1, toggleAccordion1] = useState(false);

  const logOutUser = () => {
    handleAppData(AppDataDefault);
    navigation.goBack();
  };

  if (!appData.user) {
    return <Text>Restricted view</Text>;
  }

  return (
    <LinearGradient
      colors={[theme.palette.primary1, theme.palette.secondary1]}
      style={styles.container}>
      <View style={styles.containerWrapper}>
        <View style={styles.rowSpaceBetween}>
          <Image
            style={styles.profileImg}
            source={require('../assets/images/icon-account.png')}
          />
          <View>
            <Text style={styles.userName}>{appData.user.name}</Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.closeBtn}
            onPress={() => navigation.goBack()}>
            <Icon name={'close'} size={20} color={theme.palette.default} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => toggleAccordion1(!accordion1)}
          style={styles.accordionBtn}>
          <Text style={styles.accordionBtnText}>{lang.personalInfo}</Text>
          <Icon
            name={!accordion1 ? 'arrow-right' : 'arrow-bottom'}
            size={10}
            color={theme.palette.default}
          />
        </TouchableOpacity>
        {accordion1 && (
          <View style={styles.accordionContent}>
            <Text
              style={
                styles.accordionContentText
              }>{`${lang.name}: ${appData.user.name}`}</Text>
            <Text
              style={
                styles.accordionContentText
              }>{`${lang.country}: ${appData.user.country}`}</Text>
            <Text
              style={
                styles.accordionContentText
              }>{`${lang.phone}: ${appData.user.phone}`}</Text>
            <Text
              style={
                styles.accordionContentText
              }>{`${lang.email}: ${appData.user.email}`}</Text>
          </View>
        )}
      </View>
      <View style={styles.rowCenteredContent}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => logOutUser()}
          style={styles.logOutBtn}>
          <Text style={styles.buttonText}>{lang.logOut}</Text>
        </TouchableOpacity>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 100,
    width: '100%',
    maxWidth: 300,
  },
  profileImg: {
    width: 80,
    height: 80,
  },
  userName: {
    ...theme.h3,
    color: theme.palette.default,
  },
  closeBtn: {
    width: 20,
    height: 20,
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    alignItems: 'center',
    maxHeight: 80,
    marginBottom: 50,
    width: '100%',
  },
  rowCenteredContent: {
    flex: 1,
    justifyContent: 'center',
    maxHeight: 40,
    marginBottom: 40,
  },
  accordionBtn: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: theme.palette.default,
    maxHeight: 60,
    paddingHorizontal: 10,
    width: '100%',
  },
  accordionBtnText: {
    color: theme.palette.default,
  },
  accordionContent: {
    flex: 1,
    ...theme.flexSpaceBetween,
    justifyContent: 'flex-start',
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  accordionContentText: {
    fontSize: 14,
    color: theme.palette.default,
    marginBottom: 6,
  },
  logOutBtn: {
    ...theme.button,
    opacity: 0.5,
  },
});

import React, {useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('help');

// Context
import {AppContext} from '../../AppContext';

export default ({navigation}) => {
  const {appData} = useContext(AppContext);

  const accountAction = () => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      return navigation.navigate('Account');
    }
  };
  return (
    <View style={styles.container}>
      <View style={theme.add(styles.heroContainer)}>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => navigation.navigate('Admin')}>
          <Image source={require('../assets/images/icon-info.png')} />
        </TouchableOpacity>
        <Text numberOfLines={1} style={styles.title}>
          {lang.help}
        </Text>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => accountAction()}>
          <Image source={require('../assets/images/icon-account.png')} />
        </TouchableOpacity>
      </View>
      <View style={styles.containerWrapper}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('HelpChat')}
          style={theme.add(styles.helpCardItem)}>
          <View style={styles.imageHelpCardContent}>
            <Image
              resizeMode="contain"
              style={styles.imageHelpCard}
              source={require('../assets/images/help-chat.png')}
            />
          </View>
          <View style={styles.titleHelpCardContent}>
            <Text style={styles.titleHelpCard} numberOfLines={3}>
              {lang.chatWithUs}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('HelpChat', {phonesActive: true})}
          style={theme.add(styles.helpCardItem)}>
          <View style={styles.imageHelpCardContent}>
            <Image
              resizeMode="contain"
              style={styles.imageHelpCard}
              source={require('../assets/images/help-phone.png')}
            />
          </View>
          <View style={styles.titleHelpCardContent}>
            <Text style={styles.titleHelpCard} numberOfLines={3}>
              {lang.phoneLines}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('ContactUs')}
          style={theme.add(styles.helpCardItem)}>
          <View style={styles.imageHelpCardContent}>
            <Image
              resizeMode="contain"
              style={styles.imageHelpCard}
              source={require('../assets/images/help-contact.png')}
            />
          </View>
          <View style={styles.titleHelpCardContent}>
            <Text style={styles.titleHelpCard} numberOfLines={3}>
              {lang.contactUs}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.rowSpaceBetween}>
        <View>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={theme.bottomLink}>
            <Text style={theme.bottomLinkText}>{`‹ ${lang.back}`}</Text>
          </TouchableOpacity>
        </View>
        <View />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.inverse2,
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
    paddingHorizontal: 20,
    paddingBottom: 8,
    backgroundColor: theme.palette.inverse2,
  },
  title: {
    fontSize: 25,
    color: theme.palette.primary,
    fontWeight: 'bold',
    padding: 0,
    marginHorizontal: 5,
    maxWidth: 210,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
  helpCardItem: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: theme.palette.primary2,
    width: '100%',
    borderRadius: 8,
  },
  imageHelpCardContent: {
    flex: 1,
    padding: 10,
  },
  imageHelpCard: {
    width: '100%',
  },
  titleHelpCardContent: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  titleHelpCard: {
    ...theme.h3,
    width: '100%',
    textAlign: 'center',
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
});

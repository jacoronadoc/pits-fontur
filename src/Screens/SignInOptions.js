import React, {useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  Linking,
} from 'react-native';

// Strings
import {config} from '../Services/ConfigConstants';

// Libraries
import LinearGradient from 'react-native-linear-gradient';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import useGeolocation from '../Services/useGeolocation';
import { appleAuthAndroid, AppleButton } from '@invertase/react-native-apple-authentication';
// Settings.initializeSDK();

import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import { v4 as uuid } from 'uuid'

GoogleSignin.configure({
  scopes: ['https://www.googleapis.com/auth/drive.readonly'], // [Android] what API you want to access on behalf of the user, default is email and profile
  webClientId:
    '960610207729-r6ld6jkoktgia83rt80ol7ep9vhlcmsj.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  accountName: '', // [Android] specifies an account name on the device that should be used
  iosClientId:
    '960610207729-fb16tupj019n74rm21dtvi5m34tv0s2o.apps.googleusercontent.com', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
  googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
  openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
  profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
});

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('signInUp');

// Context
import {AppContext} from '../../AppContext';

// Services
import {postData} from '../Services/dataServices';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default ({navigation}) => {
  const {textInput} = theme;
  const {appData, handleAppData} = useContext(AppContext);
  const geolocation = useGeolocation();

  const signSocialUser = async (resource, data) => {
    const response = await postData(resource, data);
    if (!response.accessToken) {
      Alert.alert(lang.signInFailedTitle, lang.signUpFailedMessage, [
        {text: lang.signUpSuccessOk, onPress: () => false},
      ]);
    } else {
      handleAppData({
        firstAccess: false,
        user: response,
      });
      navigation.navigate('Main');
    }
  };

  const facebookSignIn = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
          Alert.alert(lang.signInFailedTitle, lang.canceledMessage, [
            {text: lang.signUpSuccessOk, onPress: () => false},
          ]);
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            // handleToken('api/auth/facebook', data.accessToken.toString());

            fetch(
              `https://graph.facebook.com/v2.5/me?fields=email,name&access_token=${data.accessToken.toString()}`,
            )
              .then(response => response.json())
              .then(user => {
                const newUserData = {
                  id: user.id,
                  name: user.name,
                };

                if (user.email) {
                  newUserData.email = user.email;
                }

                signSocialUser('api/auth/mobile/facebook', newUserData);
              })
              .catch(() => {
                console.log('ERROR GETTING DATA FROM FACEBOOK');
              });
          });
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
        Alert.alert(lang.signInFailedTitle, lang.signUpFailedMessage, [
          {text: lang.signUpSuccessOk, onPress: () => false},
        ]);
      },
    );
  };

  const googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const newUserData = {
        id: userInfo.user.id,
        name: userInfo.user.name,
      };

      if (userInfo.user.email) {
        newUserData.email = userInfo.user.email;
      }

      signSocialUser('api/auth/mobile/google', newUserData);
    } catch (error) {
      console.log(error, error.code);
      Alert.alert(lang.signInFailedTitle, lang.signUpFailedMessage, [
        {text: lang.signUpSuccessOk, onPress: () => false},
      ]);

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  const appleSignIn = async () => {
    try {
      console.log("ENTRO AL APPLE")
      // Generate secure, random values for state and nonce
      const rawNonce = uuid();
      const state = uuid();

      // Configure the request
      appleAuthAndroid.configure({
        // The Service ID you registered with Apple
        clientId: 'com.example.client-android',

        // Return URL added to your Apple dev console. We intercept this redirect, but it must still match
        // the URL you provided to Apple. It can be an empty route on your backend as it's never called.
        redirectUri: 'https://example.com/auth/callback',

        // The type of response requested - code, id_token, or both.
        responseType: appleAuthAndroid.ResponseType.ALL,

        // The amount of user information requested from Apple.
        scope: appleAuthAndroid.Scope.ALL,

        // Random nonce value that will be SHA256 hashed before sending to Apple.
        nonce: rawNonce,

        // Unique state value used to prevent CSRF attacks. A UUID will be generated if nothing is provided.
        state,
      });

      // Open the browser window for user sign in
      const response = await appleAuthAndroid.signIn();

      // Send the authorization code to your backend for verification
    } catch (error) {
      console.log(error, error.code);
    }
  };

  useEffect(() => {
    // console.log(geolocation);
  }, [geolocation]);

  return (
    <LinearGradient
      colors={[theme.palette.primary1, theme.palette.secondary1]}
      style={styles.container}>
      <View style={styles.containerWrapper}>
        <View style={theme.add(textInput.container, {alignItems: 'center'})}>
          <Icon name={'logo'} size={150} color={theme.palette.default} />
        </View>
        <Text style={styles.title}>{lang.createAccount}</Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => googleSignIn()}
          style={theme.add(styles.socialButton)}>
          <Image
            style={styles.socialButtonImage}
            source={require('../assets/images/icon-google.png')}
          />
          <Text style={styles.buttonText}>{lang.buttonGoogle}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => appleSignIn()}
          style={theme.add(styles.socialButton)}>
          <Image
            style={styles.socialButtonApple}
            source={require('../assets/images/icon-apple.png')}
          />
          <Text style={styles.buttonText}>{lang.buttonApple}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Sign Up')}
          style={styles.socialButton}>
          <Image
            style={styles.socialButtonImage}
            source={require('../assets/images/icon-email.png')}
          />
          <Text style={styles.buttonText}>{lang.buttonSignUpScreen}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => facebookSignIn()}
          style={theme.add(styles.socialButton)}>
          <Image
            style={styles.socialButtonImage}
            source={require('../assets/images/icon-facebook.png')}
          />
          <Text style={styles.buttonText}>{lang.buttonFacebook}</Text>
        </TouchableOpacity>
        <Text style={styles.text}>{lang.alreadyAccount}</Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Sign In')}
          style={theme.add(theme.button)}>
          <Text style={styles.buttonText}>{lang.buttonSignIn}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.rowSpaceBetween}>
        <View>
          {!appData.firstAccess && (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={theme.bottomLink}>
              <Text style={styles.skipLinkText}>{`‹ ${lang.back}`}</Text>
            </TouchableOpacity>
          )}
          {appData.firstAccess && (
            <TouchableOpacity
              onPress={() => {
                handleAppData({
                  firstAccess: false,
                });
                navigation.navigate('Main');
              }}
              style={theme.bottomLink}>
              <Text style={styles.skipLinkText}>{`${lang.skip} ›`}</Text>
            </TouchableOpacity>
          )}
        </View>
        <View>
          <TouchableOpacity
            onPress={() => Linking.openURL(lang.privacyPolicyLink)}
            style={theme.bottomLink}>
            <Text style={styles.skipLinkText}>{lang.privacyPolicy}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.primary,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    ...theme.h2,
    color: theme.palette.default,
    marginBottom: 30,
  },
  text: {
    color: theme.palette.default,
    marginBottom: theme.palette.spacingMd,
  },
  socialButton: {
    ...theme.button,
    ...theme.flexWrap,
    width: '100%',
    minWidth: 210,
    maxWidth: 260,
    marginBottom: theme.palette.spacingMd,
    alignItems: 'center',
    alignContent: 'center',
    paddingVertical: 10,
    height: 60,
  },
  buttonText: {
    color: theme.palette.primary,
  },
  socialButtonImage: {
    position: 'absolute',
    left: 15,
  },
  socialButtonApple: {
    position: 'absolute',
    left: 15,
    width: 20,
    height: 20
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
  skipLinkText: {
    ...theme.bottomLinkText,
    color: theme.palette.default,
  },
});

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import renderer from 'react-test-renderer';

import Main from './Main';

describe('<Main />', () => {
  it('Load 5 items in tab list', () => {
    const Component = (
      <NavigationContainer>
        <Main />
      </NavigationContainer>
    );

    const tree = renderer.create(Component).toJSON();
    expect(tree.children[1].children[0].children.length).toBe(5);
  });
});

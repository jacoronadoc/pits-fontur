import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('myPits');
const locale = i18n.currentLocale();

// Components
import Card from '../Components/Card';

// Context
import {AppContext} from '../../AppContext';

// Services
import {getArcgisPoints} from '../Services/dataServices';
import {checkFavorite} from '../Services/checkFavorite';

//Methods
import {
  getFavorites,
  setFavorite,
  deleteFavorite,
} from '../Components/PitsMap.handleFavorites';

export default ({navigation}) => {
  const {HeaderScroll} = theme;
  const {appData, handleAppData} = useContext(AppContext);
  const [headerDetails, setHeaderDetails] = useState(false);
  const shadowStyle = headerDetails ? HeaderScroll.shadow : null;
  const [favoritesList, setFavoritesList] = useState([]);

  const handleScroll = event => {
    if (event.nativeEvent.contentOffset.y > 50) {
      setHeaderDetails(true);
    } else {
      setHeaderDetails(false);
    }
  };

  const favoriteAction = async (item, itemChecked) => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      const result = !itemChecked
        ? await setFavorite(item, appData.user.accessToken)
        : await deleteFavorite(itemChecked.id, appData.user.accessToken);
      if (result) {
        const favoritesList = await getFavorites(appData.user.accessToken);
        if (favoritesList) {
          handleAppData({
            ...appData,
            favorites: favoritesList,
          });
        }
      }
    }
  };

  const accountAction = () => {
    if (!appData.user) {
      return navigation.navigate('Sign In Options');
    } else {
      return navigation.navigate('Account');
    }
  };

  const detailsAction = point => navigation.navigate('Point Details', {point});

  useEffect(() => {
    if (appData.favorites.length > 0) {
      (async () => {
        let newFavoritesList = [];

        const favoriteAtractionsList = await getArcgisPoints(
          appData.favorites,
          'Atractivo',
        );

        if (favoriteAtractionsList) {
          newFavoritesList = favoriteAtractionsList;
        }

        const favoriteOperatorsList = await getArcgisPoints(
          appData.favorites,
          'Operador',
        );

        if (favoriteOperatorsList) {
          newFavoritesList = [...newFavoritesList, ...favoriteOperatorsList];
        }

        setFavoritesList(newFavoritesList);
      })();
    } else {
      setFavoritesList([]);
    }
  }, [appData.favorites]);

  return (
    <View style={styles.container}>
      <View style={theme.add(styles.heroContainer, shadowStyle)}>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={() => navigation.navigate('Admin')}>
          <Image source={require('../assets/images/icon-info.png')} />
        </TouchableOpacity>
        <Text numberOfLines={1} style={styles.title}>
          {lang.title}
        </Text>
        <TouchableOpacity
          style={styles.headerBtn}
          activeOpacity={1}
          onPress={accountAction}>
          <Image source={require('../assets/images/icon-account.png')} />
        </TouchableOpacity>
      </View>
      <ScrollView
        style={HeaderScroll.scrollView}
        onScroll={handleScroll}
        scrollEventThrottle={1}
        contentContainerStyle={HeaderScroll.scrollContainerStyle}>
        <View style={HeaderScroll.contentWrapper}>
          {favoritesList.map((item, k) => (
            <Card
              key={k}
              item={{
                ...item,
                title: item.nombre,
                description: item[`descripcion_${locale}`] || item.descripcion,
                label: item.nombre_mpio,
                image:
                  (item.multimedia && item.multimedia[0].url) ||
                  'https://picsum.photos/id/1015/300/200.jpg',
              }}
              containerStyle={styles.cardContainer}
              titleLines={2}
              titleStyle={styles.cardTitle}
              descriptionLines={1}
              descriptionStyle={styles.cardDescription}
              infoContainerStyle={styles.cardInfoContainer}
              labelStyle={styles.cardLabel}
              favoriteVisible={true}
              favoriteChecked={() => checkFavorite(appData.favorites, item)}
              favoriteMethod={favoriteAction}
              detailsMethod={detailsAction}
            />
          ))}
          {favoritesList.length <= 0 && (
            <View style={HeaderScroll.infoDescription}>
              <View style={HeaderScroll.infoIconContainer}>
                <Icon
                  style={HeaderScroll.infoIcon}
                  name="heart"
                  size={20}
                  color={theme.palette.primary2}
                />
              </View>
              <View style={HeaderScroll.infoTextContainer}>
                <Text style={HeaderScroll.infoText}>
                  {lang.infoDestinations}
                </Text>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...theme.HeaderScroll.container,
    backgroundColor: theme.palette.default,
  },
  heroContainer: {
    ...theme.HeaderScroll.heroContainer,
    paddingHorizontal: 20,
    paddingBottom: 8,
  },
  headerBtn: {},
  title: {
    fontSize: 25,
    color: theme.palette.primary,
    fontWeight: 'bold',
    padding: 0,
    marginHorizontal: 5,
    maxWidth: 210,
  },
  cardContainer: {
    width: '100%',
    height: 120,
    marginHorizontal: 0,
    marginTop: 10,
    marginBottom: 15,
  },
  cardDescription: {
    fontSize: 15,
  },
  cardTitle: {
    fontSize: 18,
  },
  cardInfoContainer: {
    paddingHorizontal: 0,
    paddingLeft: 10,
  },
  cardLabel: {
    fontSize: 15,
  },
});

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Strings
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('signInUp');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

//Resources
import KeyboardAvoidingWrapper from '../Resources/KeyboardAvoidingWrapper';

export default ({navigation}) => {
  return (
    <KeyboardAvoidingWrapper>
      <View style={styles.container}>
        <View style={styles.containerWrapper}>
          <Text style={styles.title}>{lang.contactUs}</Text>
          <View style={styles.textContainer}>
            <TextInput
              multiline
              numberOfLines={4}
              style={styles.textArea}
              placeholder={lang.contactUsPlaceholder}
              placeholderTextColor={theme.palette.inverse4}
            />
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              navigation.navigate('SignUpConfirmation', {
                title: lang.contactUsSentTitle,
                description: lang.contactUsSentMessage,
                backTo: 'Main',
              })
            }
            style={theme.add(theme.button, theme.buttonPrimary)}>
            <Text style={theme.buttonTextPrimary}>{lang.contactUsSend}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rowSpaceBetween}>
          <View>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={theme.bottomLink}>
              <Text style={theme.bottomLinkText}>{`‹ ${lang.back}`}</Text>
            </TouchableOpacity>
          </View>
          <View />
        </View>
      </View>
    </KeyboardAvoidingWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.inverse2,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    width: '85%',
    marginTop: 15,
    marginBottom: 30,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: theme.palette.default,
    ...theme.palette.shadow,
  },
  title: {
    fontSize: 25,
    color: theme.palette.primary,
    fontWeight: 'bold',
    padding: 0,
    marginHorizontal: 5,
    maxWidth: 210,
  },
  desc: {
    color: theme.palette.primary,
    fontSize: 16,
    textAlign: 'center',
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
  textArea: {
    height: 150,
    fontSize: 18,
    color: theme.palette.inverse,
  },
});

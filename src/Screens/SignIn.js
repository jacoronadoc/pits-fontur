import React, {useEffect, useState, useContext, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Alert,
  Linking,
} from 'react-native';

// Libraries
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../Iconmoon/selection.json';

// Strings
import {config} from '../Services/ConfigConstants';
import i18n from '../nls/i18n';
import theme from '../Styles/_main';

const lang = i18n.t('signInUp');

// Icons
const Icon = createIconSetFromIcoMoon(icomoonConfig);

// Context
import {AppContext} from '../../AppContext';

// Services
import {postData} from '../Services/dataServices';
import validateEmail from '../Services/validateEmail';

//Resources
import KeyboardAvoidingWrapper from '../Resources/KeyboardAvoidingWrapper';

export default ({navigation}) => {
  const {textInput} = theme;
  const {handleAppData} = useContext(AppContext);

  const [compiledData, setPostData] = useState({});

  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState('');

  const [isEmailValid, setIsEmailValid] = useState(true);

  const ref_input2 = useRef();

  const handleInputChange = (key, value) => {
    const newCompiledData = {
      ...compiledData,
      [key]: value,
    };
    setPostData(newCompiledData);
  };

  useEffect(() => {
    handleInputChange('password', password);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [password]);

  useEffect(() => {
    const valid = validateEmail(email);
    handleInputChange('email', valid ? email : null);
    setIsEmailValid(valid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [email]);

  const handleSignIn = async () => {
    const response = await postData('api/auth/mobile/signin', compiledData);
    if (!response.accessToken) {
      Alert.alert(lang.signInFailedTitle, lang.signUpFailedMessage, [
        {text: lang.signUpSuccessOk, onPress: () => false},
      ]);
    } else {
      handleAppData({
        user: response,
      });
      navigation.navigate('Main');
    }
  };

  return (
    <KeyboardAvoidingWrapper>
      <View style={styles.container}>
        <View style={styles.containerWrapper}>
          <View style={theme.add(textInput.container, {alignItems: 'center'})}>
            <Icon name={'logo'} size={120} color={theme.palette.primary} />
          </View>
          <Text style={styles.title}>{lang.buttonSignIn}</Text>
          <View style={textInput.container}>
            <TextInput
              placeholderTextColor={textInput.placeholder}
              style={
                email !== null
                  ? isEmailValid
                    ? textInput.input
                    : textInput.invalid
                  : textInput.input
              }
              onSubmitEditing={() => ref_input2.current.focus()}
              placeholder={lang.inputEmail}
              value={email}
              onChangeText={txt => {
                setEmail(txt);
              }}
            />
          </View>
          <View style={textInput.container}>
            <TextInput
              ref={ref_input2}
              secureTextEntry={true}
              placeholderTextColor={textInput.placeholder}
              style={textInput.input}
              placeholder={lang.inputPassword}
              value={password}
              onChangeText={txt => {
                setPassword(txt);
              }}
            />
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={handleSignIn}
            style={theme.add(theme.button, theme.buttonPrimary)}>
            <Text style={theme.buttonTextPrimary}>{lang.buttonSignIn}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rowSpaceBetween}>
          <View>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={theme.bottomLink}>
              <Text style={theme.bottomLinkText}>{`‹ ${lang.back}`}</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => Linking.openURL(lang.privacyPolicyLink)}
              style={theme.bottomLink}>
              <Text style={theme.bottomLinkText}>{lang.privacyPolicy}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAvoidingWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.palette.inverse2,
  },
  containerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    ...theme.h2,
    marginBottom: 30,
  },
  rowSpaceBetween: {
    ...theme.flexRow,
    ...theme.flexSpaceBetween,
    maxHeight: 30,
    width: '100%',
    marginBottom: 40,
    paddingHorizontal: 15,
  },
});

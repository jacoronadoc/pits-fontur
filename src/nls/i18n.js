import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import english from './string-EN';
import spanish from './string-ES';

i18n.translations = {
  en: english,
  es: spanish,
};

const Locales = RNLocalize.getLocales();
/*
Locales: [
  { countryCode: "GB", languageTag: "en-GB", languageCode: "en", isRTL: false },
  ...
]
*/
i18n.defaultLocale = Locales[0].languageCode;
i18n.locale = Locales[0].languageCode;
i18n.fallbacks = true;

export default i18n;

# SVG Icons

[&lsaquo; Return](../README.md)

The icon set was created with **Icomoon** to edit or add more icons do the following

- Go to [icomoon](https://icomoon.io/app/) app
- Create a new icon set (deleting all default icons in the list)
- Import the **selection.json** file from `./src/Iconmoon/`
- Add - Edit - Delete icons using the tools provided by the editor
- Download icon set and replace all contents in `./src/Iconmoon/`
- Copy the `.ttf` file inside each platform package or use `react-native link` command as it is shown in the final manual (didn't work for me)
- start using your pack importing the `react-native-vector-icons` required method (createIconSetFromIcoMoon)


Read the following link for details.

[ADD CUSTOM ICONS TO YOUR REACT NATIVE APPLICATION](https://blog.bam.tech/developer-news/add-custom-icons-to-your-react-native-application)
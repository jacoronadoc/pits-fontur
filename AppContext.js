import React, {createContext, useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';

let appDataDefault = {
  firstAccess: false,
  user: null,
  favorites: [],
};

export const AppDataDefault = appDataDefault;

export const AppContext = createContext();

export const AppContextProvider = props => {
  const [appData, setAppData] = useState(appDataDefault);

  const storeData = async value => {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem('@deviceStorageData', jsonValue);
    } catch (e) {
      // saving error
      console.log('Data in device storage not saved');
    }
  };

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@deviceStorageData');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value
      console.log('Can not get Data from device storage');
    }
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(async () => {
    const firstLoadData = await getData();
    if (!firstLoadData) {
      setAppData({
        ...appDataDefault,
        firstAccess: true,
      });
      SplashScreen.hide();
    } else {
      setAppData(firstLoadData);
      SplashScreen.hide();
    }
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(async () => {
    await storeData(appData);
  }, [appData]);

  const handleAppData = obj => {
    const newData = {
      ...appData,
      ...obj,
    };
    setAppData(newData);
  };

  return (
    <AppContext.Provider value={{appData, handleAppData}}>
      {props.children}
    </AppContext.Provider>
  );
};

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Screens Private
import Main from './src/Screens/Main';
import Admin from './src/Screens/Admin';
import Account from './src/Screens/Account';

// Screens Public
import Landing from './src/Screens/Landing';
import SignInOptions from './src/Screens/SignInOptions';
import SignIn from './src/Screens/SignIn';
import SignUp from './src/Screens/SignUp';
import SignUpConfirmation from './src/Screens/SignUpConfirmation';
import PointDetails from './src/Screens/PointDetails';
import NewsDetails from './src/Screens/NewsDetails';
import Help from './src/Screens/Help';
import HelpChat from './src/Screens/HelpChat';
import ContactUs from './src/Screens/ContactUs';

// Resources
import VideoWebView from './src/Resources/VideoWebView';
import Tour360 from './src/Components/Tour360';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Main"
          component={Main}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Landing"
          component={Landing}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Admin"
          component={Admin}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Account"
          component={Account}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Sign In Options"
          component={SignInOptions}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Sign In"
          component={SignIn}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Sign Up"
          component={SignUp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUpConfirmation"
          component={SignUpConfirmation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Point Details"
          component={PointDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="News Details"
          component={NewsDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoWebView"
          component={VideoWebView}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Image360WebView"
          component={Tour360}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Help"
          component={Help}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HelpChat"
          component={HelpChat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ContactUs"
          component={ContactUs}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
